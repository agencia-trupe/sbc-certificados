<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contatos')->insert([
            'email' => 'contato@trupe.net',
        ]);
    }
}
