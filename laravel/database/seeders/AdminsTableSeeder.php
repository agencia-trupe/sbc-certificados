<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('admins')->insert([
            'nome'     => 'admin',
            'email'    => 'teste@trupe.net',
            'password' => bcrypt('senhatrupe'),
        ]);
    }
}
