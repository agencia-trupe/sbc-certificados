<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AreaAdminTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('area_admin')->insert([
            'tempo_aviso_vencimento'  => 6,
            'texto_email_vencimento'  => '',
            'texto_email_certificado' => '',
        ]);
    }
}
