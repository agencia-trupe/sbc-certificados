<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PoliticaDePrivacidadeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('politica_de_privacidade')->insert([
            'texto'  => '',
        ]);
    }
}
