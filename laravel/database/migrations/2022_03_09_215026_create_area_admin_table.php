<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreaAdminTable extends Migration
{
    public function up()
    {
        Schema::create('area_admin', function (Blueprint $table) {
            $table->id();
            $table->integer('tempo_aviso_vencimento');
            $table->text('texto_email_vencimento');
            $table->text('texto_email_certificado');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('area_admin');
    }
}
