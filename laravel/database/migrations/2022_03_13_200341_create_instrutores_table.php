<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstrutoresTable extends Migration
{
    public function up()
    {
        Schema::create('instrutores', function (Blueprint $table) {
            $table->id();
            $table->string('hash_id')->unique();
            $table->string('cpf')->unique();
            $table->string('nome');
            $table->string('email')->unique()->nullable();
            $table->string('profissao')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('instrutores');
    }
}
