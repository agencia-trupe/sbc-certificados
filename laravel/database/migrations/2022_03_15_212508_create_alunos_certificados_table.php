<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlunosCertificadosTable extends Migration
{
    public function up()
    {
        Schema::create('alunos_certificados', function (Blueprint $table) {
            $table->id();
            $table->string('hash_id')->unique()->nullable();
            $table->foreignId('aluno_id')->constrained('alunos')->onDelete('cascade');
            $table->foreignId('certificado_id')->constrained('certificados')->onDelete('cascade');
            $table->integer('nota_prova_teorica');
            $table->integer('nota_prova_pratica');
            $table->integer('nota_final');
            $table->boolean('certificado_emitido')->default(false);
            $table->date('data_envio_certificado')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('alunos_certificados');
    }
}
