<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificadosTable extends Migration
{
    public function up()
    {
        Schema::create('certificados', function (Blueprint $table) {
            $table->id();
            $table->string('hash_id')->unique();
            $table->date('data_emissao');
            $table->date('data_validade');
            $table->foreignId('curso_id')->constrained('cursos')->onDelete('cascade');
            $table->foreignId('instituicao_id')->constrained('instituicoes')->onDelete('cascade');
            $table->string('cidade');
            $table->string('uf');
            $table->foreignId('instrutor_id')->constrained('instrutores')->onDelete('cascade');
            $table->text('equipe')->nullable();
            $table->boolean('aviso_vencimento_enviado')->default(false);
            $table->date('data_aviso_vencimento_enviado')->nullable();
            $table->string('status_emails')->default('nao_enviados');
            $table->date('data_status_emails_enviados')->nullable();
            $table->boolean('instituicao_parceira')->default(false);
            $table->string('nome_instituicao')->nullable();
            $table->string('email_instituicao')->nullable();
            $table->text('texto_instituicao')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('certificados');
    }
}
