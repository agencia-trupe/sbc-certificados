<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Painel\AceiteDeCookiesController;
use App\Http\Controllers\Painel\AdminsController;
use App\Http\Controllers\Painel\AreaAdminController;
use App\Http\Controllers\Painel\ConfiguracoesController;
use App\Http\Controllers\Painel\ContatosController;
use App\Http\Controllers\Painel\PainelController;
use App\Http\Controllers\Painel\PoliticaDePrivacidadeController;
use App\Http\Controllers\Painel\UsersController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'painel',
], function () {
    // AUTH - BREEZE
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])
        ->middleware('guest')
        ->name('login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware('guest');
    Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->middleware('guest')
        ->name('password.request');
    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest')
        ->name('password.email');
    Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->middleware('guest')
        ->name('password.reset');
    Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware('guest')
        ->name('password.update');
    Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware('auth')
        ->name('password.confirm');
    Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware('auth');
    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth')
        ->name('logout');

    // PAINEL
    Route::group([
        'middleware' => ['auth']
    ], function () {
        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('cache:clear');

            return 'DONE';
        });

        Route::get('/', [PainelController::class, 'index'])->name('painel');
        Route::post('image-upload', [PainelController::class, 'imageUpload']);
        Route::post('order', [PainelController::class, 'order']);

        Route::name('painel.')->group(function () {
            Route::resource('usuarios', UsersController::class);
            Route::resource('configuracoes', ConfiguracoesController::class)->only(['index', 'update']);
            Route::resource('politica-de-privacidade', PoliticaDePrivacidadeController::class)->only(['index', 'update']);
            Route::get('aceite-de-cookies', [AceiteDeCookiesController::class, 'index'])->name('aceite-de-cookies.index');
            Route::resource('contatos', ContatosController::class)->only(['index', 'update']);
            Route::resource('admins', AdminsController::class);
            Route::resource('area-admin', AreaAdminController::class)->only(['index', 'update']);
        });
    });
});
