<?php

use App\Http\Controllers\ConsultaController;
use App\Http\Controllers\PoliticaDePrivacidadeController;
use App\Http\Controllers\Profissionais\Auth\AuthController;
use App\Http\Controllers\Profissionais\Auth\PasswordController;
use App\Http\Controllers\Profissionais\CertificadosController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect()->route('consulta.index');
})->name('home');

Route::get('/consulta', [ConsultaController::class, 'index'])->name('consulta.index');
Route::get('/consulta/{hash_id}', [ConsultaController::class, 'show'])->name('consulta.show');
Route::get('/consulta/{hash_id}/print', [ConsultaController::class, 'showPrint'])->name('consulta.showPrint');
Route::post('/consulta', [ConsultaController::class, 'post'])->name('consulta.post');
Route::get('politica-de-privacidade', [PoliticaDePrivacidadeController::class, 'index'])->name('politica-de-privacidade');
Route::post('aceite-de-cookies', [ConsultaController::class, 'postCookies'])->name('aceite-de-cookies.post');

Route::name('profissionais.')->group(function () {
    Route::get('/cadastro', [AuthController::class, 'indexCadastro'])->name('cadastro');
    Route::post('/cadastro', [AuthController::class, 'cadastro'])->name('cadastro.post');
    Route::get('/login', [AuthController::class, 'index'])->name('login');
    Route::post('/login', [AuthController::class, 'login'])->name('login.post');
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/esqueci-a-senha', [PasswordController::class, 'index'])->name('esqueci-a-senha');
    Route::post('/esqueci-a-senha', [PasswordController::class, 'post'])->name('esqueci-a-senha.post');
    Route::get('/redefinir-senha/{token}', [PasswordController::class, 'indexReset'])->name('redefinir-senha');
    Route::post('/redefinir-senha', [PasswordController::class, 'postReset'])->name('redefinir-senha.post');

    Route::group([
        'middleware' => ['auth.profissionais'],
        'guest' => ['guest.profissionais']
    ], function () {
        Route::get('/{aluno:id}/editar-cadastro', [AuthController::class, 'edit'])->name('edit');
        Route::post('/{aluno:id}/editar-cadastro', [AuthController::class, 'update'])->name('update');

        Route::get('/certificados', [CertificadosController::class, 'index'])->name('certificados');
        Route::get('/certificados/{certificado:hash_id}', [CertificadosController::class, 'show'])->name('certificados.show');
        Route::get('/certificados/{certificado:hash_id}/print', [CertificadosController::class, 'showPrint'])->name('certificados.showPrint');
    });
});


require __DIR__ . '/auth-admin.php';
require __DIR__ . '/auth-painel.php';
