<?php

use App\Http\Controllers\Admin\AlunosCertificadosController;
use App\Http\Controllers\Admin\AlunosController;
use App\Http\Controllers\Admin\Auth\AuthController;
use App\Http\Controllers\Admin\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Admin\Auth\PasswordController;
use App\Http\Controllers\Admin\BuscaController;
use App\Http\Controllers\Admin\CertificadosController;
use App\Http\Controllers\Admin\CursosController;
use App\Http\Controllers\Admin\InstituicoesController;
use App\Http\Controllers\Admin\InstrutoresController;
use App\Http\Controllers\Admin\RelatoriosController;
use App\Http\Controllers\Admin\VisaoGeralController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'admin',
], function () {
    Route::name('admin.')->group(function () {
        Route::get('/login', [AuthController::class, 'index'])->name('login');
        Route::post('/login', [AuthController::class, 'login'])->name('login.post');
        Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
        Route::get('/esqueci-a-senha', [PasswordController::class, 'index'])->name('esqueci-a-senha');
        Route::post('/esqueci-a-senha', [PasswordController::class, 'post'])->name('esqueci-a-senha.post');
        Route::get('/redefinir-senha/{token}', [PasswordController::class, 'indexReset'])->name('redefinir-senha');
        Route::post('/redefinir-senha', [PasswordController::class, 'postReset'])->name('redefinir-senha.post');

        // admin logado
        Route::group([
            'middleware' => ['auth.admin'],
            'guest' => ['guest.admin']
        ], function () {
            Route::get('/{admin:id}/editar-cadastro', [AuthController::class, 'edit'])->name('edit');
            Route::post('/{admin:id}/editar-cadastro', [AuthController::class, 'update'])->name('update');

            Route::get('/', function() {
                return redirect()->route('admin.visaoGeral.index');
            })->name('home');
            // VISÃO GERAL
            Route::get('/visao-geral', [VisaoGeralController::class, 'index'])->name('visaoGeral.index');
            Route::get('/visao-geral/{certificado:hash_id}', [VisaoGeralController::class, 'show'])->name('visaoGeral.show');
            Route::post('/visao-geral/{certificado:hash_id}/aviso-de-vencimento', [VisaoGeralController::class, 'sendEmailsVencimento'])->name('visaoGeral.sendEmailsVencimento');
            Route::post('/visao-geral/{certificado:hash_id}/aviso-de-vencimento/editar', [VisaoGeralController::class, 'updateDataEnvioVencimento'])->name('visaoGeral.updateDataEnvioVencimento');
            Route::get('/certificados/{certificado:hash_id}/extrair-mailing', [VisaoGeralController::class, 'extrairMailing'])->name('visaoGeral.extrairMailing');
            // ALUNOS
            Route::get('/alunos', [AlunosController::class, 'index'])->name('alunos.index');
            Route::post('/alunos/importar-dados', [AlunosController::class, 'importExcel'])->name('alunos.import');
            Route::get('/alunos/cadastro', [AlunosController::class, 'create'])->name('alunos.create');
            Route::post('/alunos/cadastro', [AlunosController::class, 'store'])->name('alunos.store');
            Route::get('/alunos/{aluno:hash_id}/certificados', [AlunosController::class, 'show'])->name('alunos.show');
            Route::get('/alunos/{aluno:hash_id}/certificados/{certificado:hash_id}/reenviar', [AlunosController::class, 'reenviarCertificado'])->name('alunos.reenviarCertificado');
            Route::get('/alunos/{aluno:hash_id}/editar', [AlunosController::class, 'edit'])->name('alunos.edit');
            Route::post('/alunos/{aluno:hash_id}/editar', [AlunosController::class, 'update'])->name('alunos.update');
            Route::get('/alunos/{aluno:hash_id}/excluir', [AlunosController::class, 'delete'])->name('alunos.delete');
            // INSTRUTORES
            Route::get('/instrutores', [InstrutoresController::class, 'index'])->name('instrutores.index');
            Route::get('/instrutores/cadastro', [InstrutoresController::class, 'create'])->name('instrutores.create');
            Route::post('/instrutores/cadastro', [InstrutoresController::class, 'store'])->name('instrutores.store');
            Route::get('/instrutores/cadastro/{instrutor:hash_id}', [InstrutoresController::class, 'edit'])->name('instrutores.edit');
            Route::post('/instrutores/cadastro/{instrutor:hash_id}', [InstrutoresController::class, 'update'])->name('instrutores.update');
            Route::get('/instrutores/cadastro/{instrutor:hash_id}/excluir', [InstrutoresController::class, 'delete'])->name('instrutores.delete');
            // INSTITUIÇÕES
            Route::get('/instituicoes', [InstituicoesController::class, 'index'])->name('instituicoes.index');
            Route::get('/instituicoes/cadastro', [InstituicoesController::class, 'create'])->name('instituicoes.create');
            Route::post('/instituicoes/cadastro', [InstituicoesController::class, 'store'])->name('instituicoes.store');
            Route::get('/instituicoes/cadastro/{instituicao:hash_id}', [InstituicoesController::class, 'edit'])->name('instituicoes.edit');
            Route::post('/instituicoes/cadastro/{instituicao:hash_id}', [InstituicoesController::class, 'update'])->name('instituicoes.update');
            Route::get('/instituicoes/cadastro/{instituicao:hash_id}/excluir', [InstituicoesController::class, 'delete'])->name('instituicoes.delete');
            // CURSOS
            Route::get('/cursos', [CursosController::class, 'index'])->name('cursos.index');
            Route::get('/cursos/cadastro', [CursosController::class, 'create'])->name('cursos.create');
            Route::post('/cursos/cadastro', [CursosController::class, 'store'])->name('cursos.store');
            Route::get('/cursos/cadastro/{curso:hash_id}', [CursosController::class, 'edit'])->name('cursos.edit');
            Route::post('/cursos/cadastro/{curso:hash_id}', [CursosController::class, 'update'])->name('cursos.update');
            Route::get('/cursos/cadastro/{curso:hash_id}/excluir', [CursosController::class, 'delete'])->name('cursos.delete');
            // CERTIFICADOS
            Route::get('/certificados', [CertificadosController::class, 'index'])->name('certificados.index');
            Route::get('/certificados/cadastro', [CertificadosController::class, 'create'])->name('certificados.create');
            Route::post('/certificados/cadastro', [CertificadosController::class, 'store'])->name('certificados.store');
            Route::get('/certificados/{certificado:hash_id}/editar', [CertificadosController::class, 'edit'])->name('certificados.edit');
            Route::post('/certificados/{certificado:hash_id}/editar', [CertificadosController::class, 'update'])->name('certificados.update');
            Route::post('/certificados/{certificado:hash_id}/enviar-emails', [CertificadosController::class, 'sendEmails'])->name('certificados.sendEmails');
            Route::get('/certificados/{certificado:hash_id}/extrair-relatorio', [CertificadosController::class, 'extrairRelatorio'])->name('certificados.extrairRelatorio');
            // CERTIFICADOS-alunos
            Route::get('/certificados/aluno/{cpf}', [AlunosCertificadosController::class, 'findAluno'])->name('certificados.findAluno');
            Route::post('/certificados/aluno/adicionar/', [AlunosCertificadosController::class, 'addAlunoCertificado'])->name('certificados.addAlunoCertificado');
            Route::delete('/certificados/{certificadoId}/aluno/{alunoId}/excluir/', [AlunosCertificadosController::class, 'deleteAlunoCertificado'])->name('certificados.deleteAlunoCertificado');
            // BUSCA
            Route::post('busca', [BuscaController::class, 'search'])->name('search');
            // RELATÓRIOS
            Route::get('relatorios', [RelatoriosController::class, 'index'])->name('relatorios.index');
            Route::post('relatorios/totais', [RelatoriosController::class, 'getTotais'])->name('relatorios.getTotais');
            Route::post('relatorios/filtros', [RelatoriosController::class, 'postFiltros'])->name('relatorios.filtros');
            Route::get('/relatorios/filtros/{dataInicio}/{dataFim}/{filtro}/extrair', [RelatoriosController::class, 'extrairRelatorio'])->name('relatorios.extrairRelatorio');
        });
    });
});
