@extends('layouts.template')

@section('content')

<main class="login">

    <div class="externa">
        <div class="imagem">
            <div class="img"></div>
        </div>
        <div class="father">
            <h1>Sistema E-cards SBC</h1>
            <p>• CONSULTA DE CERTIFICADOS •</p>

            <div class="logs2b">
                <form action="{{ route('consulta.post') }}" method="post">
                    {!! csrf_field() !!}
                    <label for="search_c">Informe o código do Certificado para consulta:</label>
                    <input type="text" id="search_c" name="codigo_certificado" required>

                    <button type="submit">CONSULTAR</button>

                    @include('layouts.flash')
                </form>
            </div>
        </div>
    </div>

    </section>


    @endsection