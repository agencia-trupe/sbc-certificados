@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="instrutores">

    <div class="center">

        <div class="ficha">
            <div class="cabec">
                @if(isset($instrutor))
                <h1>INSTRUTORES | EDITAR CADASTRO</h1>
                @else
                <h1>INSTRUTORES | NOVO CADASTRO</h1>
                @endif
            </div>

            <div class="card">
                @if(isset($instrutor))
                <form action="{{ route('admin.instrutores.update', $instrutor->hash_id) }}" method="post" class="wrapper">
                    @else
                    <form action="{{ route('admin.instrutores.store') }}" method="post" class="wrapper">
                        @endif
                        {!! csrf_field() !!}
                        <div class="campo">
                            <label for="f1_instrutor" class="label">ID:</label>
                            <input type="text" id="f1_instrutor" @if(isset($instrutor)) value="{{ $instrutor->hash_id }}" @else value="" @endif placeholder="(gerado automaticamente)" disabled>
                        </div>
                        <div class="campo">
                            <label for="cpf" class="label">CPF:</label>
                            <input type="text" id="cpf" name="cpf" class="input-cpf" value="{{ isset($instrutor) ? $instrutor->cpf : '' }}">
                        </div>
                        <div class="campo">
                            <label for="nome" class="label">Nome do Instrutor | Diretor do Curso:</label>
                            <input type="text" id="nome" name="nome" value="{{ isset($instrutor) ? $instrutor->nome : '' }}">
                        </div>
                        <div class="campo">
                            <label for="email" class="label">E-mail:</label>
                            <input type="text" id="email" name="email" value="{{ isset($instrutor) ? $instrutor->email : '' }}">
                        </div>
                        <div class="campo">
                            <label for="profissao" class="label">Profissão:</label>
                            <input type="text" id="profissao" name="profissao" value="{{ isset($instrutor) ? $instrutor->profissao : '' }}">
                        </div>
                        <div class="botoes">
                            @if(isset($instrutor))
                            <button type="submit" class="button_insere">ALTERAR</button>
                            @else
                            <button type="submit" class="button_insere">INSERIR</button>
                            @endif
                            <a href="{{ route('admin.instrutores.index') }}" class="button_voltar">CANCELAR</a>
                        </div>

                        @include('layouts.flash')
                    </form>
            </div>
        </div>
    </div>

</main>



@endsection