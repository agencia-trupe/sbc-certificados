@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="instrutores">

    <div class="center">

        @include('layouts.flash')

        <div class="tabela">
            <div class="cabec">
                <h1>INSTRUTORES</h1>
                <a href="{{ route('admin.instrutores.create') }}">
                    <div class="cadastro">
                        NOVO CADASTRO
                    </div>
                </a>
            </div>
            <div>
                <hr>
                @if(count($instrutores) > 0)
                @foreach($instrutores as $instrutor)
                <div class="line">
                    <div class="flex">
                        <p class="name">{{ $instrutor->hash_id }}</p>
                        <span>|</span>
                        <p class="name">{{ $instrutor->nome }}</p>
                    </div>
                    <div class="botoes">
                        <div class="btn-edit">
                            <a href="{{ route('admin.instrutores.edit', $instrutor->hash_id) }}"><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" class="editar" alt=""></a>
                        </div>
                        <div class="btn-delete">
                            @if(count($certificados->where('instrutor_id', $instrutor->id)) == 0)
                            <a href="{{ route('admin.instrutores.delete', $instrutor->hash_id) }}" onclick="return confirm('Tem certeza que deseja excluir o instrutor selecionado?')"><img src="{{ asset('assets/img/layout/x-sair.svg')}}" class="sair" alt=""></a>
                            @endif
                        </div>
                    </div>
                </div>

                <hr>

                @endforeach

                @else
                <div class="line">
                    <p class="text">Nenhum registro encontrado.</p>
                </div>
                <hr>
                @endif
            </div>
        </div>

        <div class="pages">
            {!! $instrutores->links() !!}
        </div>

    </div>
</main>

@endsection