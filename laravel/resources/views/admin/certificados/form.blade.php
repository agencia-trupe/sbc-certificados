@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="turmas">

    <section class="cert_novo">
        <div class="center">
            <div class="ficha">
                @if(isset($certificado))
                <h1>CERTIFICADOS | EDITAR CADASTRO</h1>
                @else
                <h1>CERTIFICADOS | NOVO CADASTRO</h1>
                @endif
                <div class="interna">
                    @if(isset($certificado))
                    <form action="{{ route('admin.certificados.update', $certificado->hash_id) }}" method="post" class="form-certificado" id="{{ $certificado->hash_id }}">
                        @else
                        <form action="{{ route('admin.certificados.store') }}" method="post">
                            @endif
                            {!! csrf_field() !!}
                            <div class="linha1">
                                <div class="campo field1">
                                    <label for="f1_certificado">ID do Certificado:</label>
                                    <input type="text" id="f1_certificado" @if(isset($certificado)) value="{{ $certificado->hash_id }}" @else value="" @endif placeholder="(gerado automaticamente)" disabled>
                                </div>
                                <div class="campo field2">
                                    <label for="data_emissao">Data da Emissão:</label>
                                    <input type="date" name="data_emissao" id="data_emissao" @if(isset($certificado)) value="{{ $certificado->data_emissao->format('Y-m-d') }}" @else value="" @endif required>
                                </div>
                                <div class="campo field2">
                                    <label for="data_validade">Validade:</label>
                                    <input type="date" name="data_validade" id="data_validade" @if(isset($certificado)) value="{{ $certificado->data_validade->format('Y-m-d') }}" @else value="" @endif required>
                                </div>
                            </div>

                            <div class="linha2">
                                <div class="campo field1">
                                    <label for="curso_id">Curso:</label>
                                    <select name="curso_id" id="curso_id">
                                        <option value="selecione" selected>Selecionar CURSO</option>
                                        @foreach($cursos as $curso)
                                        <option value="{{ $curso->hash_id }}" @if(isset($certificado) && $certificado->curso_id == $curso->id) selected @endif>{{ $curso->titulo }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="linha2">
                                <div class="campo field1">
                                    <label for="instituicao_id">Instituição:</label>
                                    <select name="instituicao_id" id="instituicao_id">
                                        <option value="selecione" selected>Selecionar INSTITUIÇÃO</option>
                                        @foreach($instituicoes as $instituicao)
                                        <option value="{{ $instituicao->hash_id }}" @if(isset($certificado) && $certificado->instituicao_id == $instituicao->id) selected @endif>{{ $instituicao->nome }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="linha_new">
                                <div class="campo field2">
                                    <label for="cidade">Cidade:</label>
                                    <input type="text" class="cidade" id="cidade" name="cidade" @if(isset($certificado)) value="{{ $certificado->cidade }}" @else value="" @endif required>
                                </div>
                                <div class="campo field2">
                                    <label for="uf">UF:</label>
                                    <input type="text" class="uf" id="uf" name="uf" @if(isset($certificado)) value="{{ $certificado->uf }}" @else value="" @endif required>
                                </div>
                            </div>

                            <div class="linha2">
                                <div class="campo field1">
                                    <label for="instrutor_id">ID | Diretor do Curso | Instrutor</label>
                                    <select name="instrutor_id" id="instrutor_id">
                                        <option value="selecione" selected>Selecionar INSTRUTOR</option>
                                        @foreach($instrutores as $instrutor)
                                        <option value="{{ $instrutor->hash_id }}" @if(isset($certificado) && $certificado->instrutor_id == $instrutor->id) selected @endif>{{ $instrutor->hash_id }} | {{ $instrutor->nome }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="linha3">
                                <div class="campo field1">
                                    <label for="equipe">Equipe:</label>
                                    <textarea id="equipe" name="equipe" placeholder="(cadastrar nomes separados por quebra de linha)">@if(isset($certificado)){{ $certificado->equipe }}@endif</textarea>
                                </div>
                            </div>

                            @if(isset($certificado))
                            <div class="linha4 form-add-aluno-certificado">
                                <div class="campo field1">
                                    <meta name="_token" content="{{ csrf_token() }}">
                                    <input type="hidden" name="certificado_id" class="certificado" value="{{ $certificado->hash_id }}">
                                    <label for="cpf">Alunos aprovados:</label>
                                    <div class="busca" style="display: flex;">
                                        <input type="text" class="input1 input-cpf input-buscar-cpf" name="cpf" value="">
                                        <a href="" class="link-buscar-cpf">BUSCAR</a>
                                    </div>
                                    <input type="text" class="input1 input-aluno" name="aluno" value="" disabled>
                                </div>
                                <div class="campo field2">
                                    <label for="nota_prova_teorica" class="label1">nota prova <br>teórica</label>
                                    <input type="number" class="input2 nota-teorica" name="nota_prova_teorica" min=0 max=100 value="">
                                </div>
                                <div class="campo field2">
                                    <label for="nota_prova_pratica" class="label1">nota prova <br>prática</label>
                                    <input type="number" class="input2 nota-pratica" name="nota_prova_pratica" min=0 max=100 value="">
                                </div>
                                <div class="campo field2">
                                    <label for="nota_final" class="label1">nota final</label>
                                    <input type="number" class="input2 nota-final" name="nota_final" min=0 max=100 value="">
                                </div>

                                <!-- ajax post para inserir alunos_certificados (certificado_emitido = 0) -->
                                <a href="" class="link-add-aluno-certificado">
                                    <div class="button">
                                        INSERIR
                                    </div>
                                </a>
                            </div>

                            <div class="results resultados-alunos-certificados">
                                @if(!empty($alunosCertificados))
                                @foreach($alunosCertificados as $aluno)
                                <div class="newLine" id="{{ $aluno->id_ac }}">
                                    <p>{{ $aluno->nome }}</p>
                                    <p>{{ $aluno->email }}</p>
                                    <div class="flex2">
                                        <p>{{ $aluno->nota_prova_teorica }}</p>
                                        |
                                        <p>{{ $aluno->nota_prova_pratica }}</p>
                                        |
                                        <p>{{ $aluno->nota_final }}</p>
                                    </div>
                                    @if($aluno->certificado_emitido == 1 && $aluno->data_envio_certificado != null)
                                    <p class="status-emails">[ e-mail enviado {{ $aluno->data_envio_certificado->format('d/m/Y') }} ]</p>
                                    @else
                                    <p class="email status-emails">[ ENVIO DE E-MAIL PENDENTE ]</p>
                                    <a href="" class="link-excluir-aluno-certificado" hashId="{{ $aluno->hash_id }}"><img src="{{ asset('assets/img/layout/x-sair.svg')}}" class="sair" alt=""></a>
                                    @endif
                                </div>
                                @endforeach
                                @endif
                            </div>
                            @endif

                            <div class="hr" style="margin-top: 20px;"></div>

                            <div class="linha5">
                                <input type="checkbox" id="instituicao_parceira" name="instituicao_parceira" class="div4_checkbox_label container_checkbox" @if(isset($certificado) && $certificado->instituicao_parceira == 1) checked @endif>
                                <span class="checkmark"></span>
                                <label for="instituicao_parceira" class="div4_checkbox_label container_checkbox"> Curso realizado para Instituição parceira</label>
                            </div>

                            <div class="linha6 dados-instituicao-parceira" @if(isset($certificado) && $certificado->instituicao_parceira == 1) style="display: block;" @else style="display: none;" @endif>
                                <div class="flex3">
                                    <input type="text" name="nome_instituicao" placeholder="Nome da instituição parceira" @if(isset($certificado)) value="{{ $certificado->nome_instituicao }}" @else value="" @endif>
                                    <input type="text" name="email_instituicao" placeholder="E-mail para envio do relatório" @if(isset($certificado)) value="{{ $certificado->email_instituicao }}" @else value="" @endif>
                                </div>
                                <textarea name="texto_instituicao" id="texto_instituicao" cols="30" rows="10" placeholder="Texto do e-mail de encaminhamento do Certificado.">@if(isset($certificado)){{ $certificado->texto_instituicao }}@endif</textarea>
                            </div>

                            @if(isset($certificado))
                            <div class="botoes_vermelhos">
                                <a href="" class="link-enviar-certificados-email" hashId="{{ $certificado->hash_id }}">
                                    <div class="button_ver1">
                                        ENVIAR CERTIFICADOS POR E-MAIL
                                        <img src="{{ asset('assets/img/layout/ico-enviar-email.svg')}}" alt="">
                                    </div>
                                </a>
                                <a href="{{ route('admin.certificados.extrairRelatorio', $certificado->hash_id) }}" onclick="return confirm('Deseja fazer o download do relatório?')">
                                    <div class="button_ver1">
                                        EXTRAIR RELATÓRIO DE CERTIFICADOS
                                        <img src="{{ asset('assets/img/layout/ico-download.svg')}}" alt="">
                                    </div>
                                </a>
                            </div>
                            @endif

                            <div class="flex">
                                @if(isset($certificado))
                                <button type="submit" class="novo_cert">ATUALIZAR CERTIFICADO</button>
                                @else
                                <button type="submit" class="novo_cert">CRIAR NOVO CERTIFICADO</button>
                                @endif
                                <a href="{{ route('admin.certificados.index') }}">
                                    <div class="button_voltar">
                                        CANCELAR
                                    </div>
                                </a>
                            </div>

                            <div class="flash flash-ajax" style="display: none;"></div>

                            @include('layouts.flash')
                        </form>
                </div>
            </div>
        </div>
    </section>

</main>

@endsection