@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="turmas">

    <div class="center">

        @include('layouts.flash')

        <div class="global">
            <form action="{{ route('admin.certificados.index') }}" method="get" class="filtro-data">
                <input type="date" name="data" id="data" value="{{ isset($data) ? $data : '' }}">
                <button type="submit">BUSCAR</button>
            </form>
            <div class="flex">
                <h1>CERTIFICADOS</h1>
                <a href="{{ route('admin.certificados.create') }}">
                    <div class="cadastro">
                        NOVO CADASTRO
                    </div>
                </a>
            </div>

            <div>
                <hr>
                @if(count($certificados) > 0)
                @foreach($certificados as $certificado)
                <div class="line">
                    <div class="flex">
                        <p class="date">{{ $certificado->data_emissao->format('d/m/Y') }}</p>
                        <span>|</span>
                        <p class="id">{{ $certificado->hash_id }}</p>
                        <span>|</span>
                        <p class="title">{{ $certificado->curso }}</p>
                    </div>
                    <div class="flex2">
                        <a href="" class="link">
                            @if($certificado->status_emails == 'enviados')
                            <p>[ E-MAILS ENVIADOS - {{ $certificado->data_status_emails_enviados->format('d/m/Y') }} ]</p>
                            @endif
                            @if($certificado->status_emails == 'nao_enviados')
                            <p>[ ENVIO TOTAL PENDENTE ]</p>
                            @endif
                            @if($certificado->status_emails == 'emails_pendentes')
                            <p>[ E-MAILS PENDENTES ]</p>
                            @endif
                        </a>
                        <div class="manager">
                            <div class="btn-edit">
                                <a href="{{ route('admin.certificados.edit', $certificado->hash_id) }}"><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" class="editar" alt=""></a>
                            </div>
                            <div class="btn-delete">
                                @if($certificado->status_emails == 'nao_enviados')
                                <a href=""><img src="{{ asset('assets/img/layout/x-sair.svg')}}" class="sair" alt=""></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                @endforeach

                @else
                <div class="line">
                    <p class="text">Nenhum registro encontrado.</p>
                </div>
                <hr>
                @endif
            </div>

            <div class="pages">
                {!! $certificados->links() !!}
            </div>

        </div>
    </div>

</main>

@endsection