@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="relatorios">

    <div class="center2">

        @include('layouts.flash')

        <div class="tabela">
            <div class="cabec">
                <h1>RELATÓRIOS</h1>
            </div>

            <div class="filtros">
                <form action="{{ route('admin.relatorios.filtros') }}" method="post" class="relatorio">
                    {!! csrf_field() !!}
                    <meta name="_token" content="{{ csrf_token() }}">
                    <div class="datas">
                        <div class="campo-data">
                            <label for="data_inicio">Data de Início:</label>
                            <input type="date" name="data_inicio" id="data_inicio" required>
                        </div>
                        <div class="campo-data">
                            <label for="data_fim">Data de Fim:</label>
                            <input type="date" name="data_fim" id="data_fim" required>
                        </div>
                        <button type="button" class="btn-consultar-totais">CONSULTAR TOTAIS</button>
                    </div>
                    <div class="radios">
                        <input type='radio' id='filtro_cursos' name='filtro' value='cursos' checked />
                        <label for="filtro_cursos">por Curso</label>
                        <input type='radio' id='filtro_instituicoes' name='filtro' value='instituicoes' />
                        <label for="filtro_instituicoes">por Instituição</label>
                    </div>
                    <div class="campo select-cursos">
                        <select name="curso_id" id="curso_id">
                            <option value="" selected>Selecionar CURSO</option>
                            @foreach($cursos as $curso)
                            <option value="{{ $curso->hash_id }}">{{ $curso->titulo }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="campo select-instituicoes" style="display: none;">
                        <select name="instituicao_id" id="instituicao_id">
                            <option value="" selected>Selecionar INSTITUIÇÃO</option>
                            @foreach($instituicoes as $instituicao)
                            <option value="{{ $instituicao->hash_id }}">{{ $instituicao->nome }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="botoes">
                        <button type="submit" class="btn-relatorio">GERAR RELATÓRIO</button>
                        <a href="{{ route('admin.relatorios.index') }}" class="link-limpar">
                            <div class="button">
                                LIMPAR
                            </div>
                        </a>
                        <a href="{{ route('admin.visaoGeral.index') }}" class="link-voltar">
                            <div class="button">
                                VOLTAR
                            </div>
                        </a>
                    </div>
                </form>
            </div>

            <div class="resultados relatorio">
                @if(isset($relatorioFiltro) && $relatorioFiltro == true)

                @if(!empty($cursoHashId))
                <h1>RELATÓRIO POR CURSO</h1>
                @elseif(!empty($instituicaoHashId))
                <h1>RELATÓRIO POR INSTITUIÇÃO</h1>
                @else
                <h1>RELATÓRIO</h1>
                @endif
                <p class="periodo">de: <span>{{ strftime("%d/%m/%Y", strtotime($dataInicio)) }}</span> a <span>{{ strftime("%d/%m/%Y", strtotime($dataFim)) }}</span></p>
                <p class="titulo">TOTAL DE CURSOS/TURMAS MINISTRADOS NO PERÍODO: <span style="font-size: 18px;">{{ $totalCertificados }} cursos</span></p>
                <p class="titulo">TOTAL DE ALUNOS EM TODOS OS CURSOS NO PERÍODO: <span style="font-size: 18px;">{{ $totalAlunos }} alunos</span></p>

                <div class="tabela">
                    @if(count($certificados) > 0)
                    <div class="header">
                        <p class="curso">ID | CURSO</p>
                        <p class="instituicao">INSTITUIÇÃO</p>
                        <p class="cidade">CIDADE - UF</p>
                        <p class="data">DATA</p>
                        <p class="n-alunos">ALUNOS</p>
                    </div>
                    @foreach($certificados as $certificado)
                    <div class="line">
                        <p class="curso">{{ $certificado->curso_nome }}</p>
                        <p class="instituicao">{{ $certificado->instituicao_nome }}</p>
                        <p class="cidade">{{ $certificado->cidade }}, {{ $certificado->uf }}</p>
                        <p class="data">{{ strftime("%d/%m/%Y", strtotime($certificado->data_emissao)) }}</p>
                        <p class="n-alunos">{{ $certificado->totalAlunos }}</p>
                    </div>
                    <hr>
                    @endforeach
                    <div class="line">
                        <p class="total">TOTAL DE ALUNOS: {{ $totalAlunos }}</p>
                    </div>

                    @if(isset($cursoHashId) && isset($instituicaoHashId))
                    <a href="{{ route('admin.relatorios.extrairRelatorio', ['dataInicio' => $dataInicio, 'dataFim' => $dataFim, 'filtro' => $filtro, 'cursoHashId' => $cursoHashId, 'instituicaoHashId' => $instituicaoHashId]) }}" onclick="return confirm('Deseja fazer o download do relatório?')" class="link-extrair-planilha">
                        EXTRAIR PLANILHA COM ALUNOS
                    </a>
                    @elseif (isset($cursoHashId) && !isset($instituicaoHashId))
                    <a href="{{ route('admin.relatorios.extrairRelatorio', ['dataInicio' => $dataInicio, 'dataFim' => $dataFim, 'filtro' => $filtro, 'cursoHashId' => $cursoHashId, 'instituicaoHashId' => '']) }}" onclick="return confirm('Deseja fazer o download do relatório?')" class="link-extrair-planilha">
                        EXTRAIR PLANILHA COM ALUNOS
                    </a>
                    @elseif (!isset($cursoHashId) && isset($instituicaoHashId))
                    <a href="{{ route('admin.relatorios.extrairRelatorio', ['dataInicio' => $dataInicio, 'dataFim' => $dataFim, 'filtro' => $filtro, 'cursoHashId' => '', 'instituicaoHashId' => $instituicaoHashId]) }}" onclick="return confirm('Deseja fazer o download do relatório?')" class="link-extrair-planilha">
                        EXTRAIR PLANILHA COM ALUNOS
                    </a>
                    @else
                    <a href="{{ route('admin.relatorios.extrairRelatorio', ['dataInicio' => $dataInicio, 'dataFim' => $dataFim, 'filtro' => $filtro, 'cursoHashId' => '', 'instituicaoHashId' => '']) }}" onclick="return confirm('Deseja fazer o download do relatório?')" class="link-extrair-planilha">
                        EXTRAIR PLANILHA COM ALUNOS
                    </a>
                    @endif

                    @else
                    <div class="line">
                        <p class="text">Nenhum registro encontrado.</p>
                    </div>
                    <hr>
                    @endif
                </div>
                @endif
                <div class="flash flash-ajax" style="display: none;"></div>
            </div>

            <div class="resultados totais" style="display: none;">
                <h1>RELATÓRIO CONSULTA DE TOTAIS</h1>
                <p class="periodo">de: <span class="inicio"></span> a <span class="fim"></span></p>

                <div class="line">
                    <p class="titulo n-certificados">TOTAL DE CURSOS/TURMAS MINISTRADOS NO PERÍODO: <span></span></p>
                    <hr>
                </div>
                <div class="line">
                    <p class="titulo n-alunos-cursos">TOTAL DE ALUNOS EM TODOS OS CURSOS NO PERÍODO: <span></span></p>
                    <hr>
                </div>
                <div class="line">
                    <p class="titulo n-instituicoes">TOTAL DE INSTIUIÇÕES COM CURSOS NO PERÍODO: <span></span></p>
                    <hr>
                </div>
                <div class="line">
                    <p class="titulo n-vencimentos">TOTAL DE CURSOS/TURMAS COM VENCIMENTO NO PERÍODO: <span></span></p>
                    <hr>
                </div>
                <div class="line">
                    <p class="titulo media-alunos">MÉDIA DE ALUNOS POR CURSOS/TURMAS NO PERÍODO: <span></span></p>
                    <hr>
                </div>
                <div class="flash flash-ajax" style="display: none;"></div>
            </div>
        </div>
    </div>
</main>

@endsection