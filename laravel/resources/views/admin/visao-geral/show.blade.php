@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main>
    <div class="geral">
        <div class="certificado_venc">

            <div class="card1">
                <h1>CERTIFICADOS COM VENCIMENTO PRÓXIMO</h1>
                <hr>
                <h4>{{ $certificadoInfos->curso }}</h4>
                <div class="flex">
                    <p class="label">INSTRUTOR: </p>
                    <p class="dados">{{ $certificadoInfos->instrutor_id }} | {{ $certificadoInfos->instrutor_nome }}</p>
                </div>
                <div class="flex">
                    <p class="label">DATA DE EMISSÃO: </p>
                    <p class="dados">{{ $certificadoInfos->data_emissao->format('d/m/Y') }}</p>
                </div>
                <div class="flex">
                    <p class="label">LOCAL: </p>
                    <p class="dados">{{ $certificadoInfos->cidade }} - {{ $certificadoInfos->uf }}</p>
                </div>
                <br>
                <div class="flex">
                    <p class="label">VALIDADE: </p>
                    <p class="dados">{{ $certificadoInfos->data_validade->format('d/m/Y') }}</p>
                </div>
                <a href="{{ route('admin.visaoGeral.extrairMailing', $certificadoInfos->hash_id) }}" onclick="return confirm('Deseja fazer o download do relatório?')">
                    <div class="button">
                        EXTRAIR MAILING DE ALUNOS <img src="{{ asset('assets/img/layout/ico-extrair-mailing.svg')}}" alt="">
                    </div>
                </a>
            </div>

            @if($certificadoInfos->aviso_vencimento_enviado == 0)
            <div class="card2">
                <h3>AVISO DE VENCIMENTO</h3>
                <p>ESTE CERTIFICADO AINDA NÃO TEVE DISPARO DE AVISO DE VENCIMENTO MARCADO COMO ENVIADO</p>
                <a href="" class="link-enviar-emails-vencimento" hashId="{{ $certificadoInfos->hash_id }}">
                    <div class="button">
                        ENVIAR AVISO DE VENCIMENTO
                    </div>
                </a>

                <a href="" class="link-adicionar-data-do-envio">
                    <div class="button">
                        ADICIONAR DATA DO ENVIO MANUAL
                    </div>
                </a>

                <div class="flex input-data-aviso-de-vencimento" style="display:none;">
                    <meta name="_token" content="{{ csrf_token() }}">
                    <input type="date" name="data_aviso_vencimento_enviado" id="data_aviso_vencimento_enviado" value="{{ date('Y-m-d') }}">
                </div>

                <div class="flex input-data-aviso-de-vencimento adicionar" style="display:none;">
                    <meta name="_token" content="{{ csrf_token() }}">
                    <input type="date" name="data_aviso_vencimento_enviado" id="data_aviso_vencimento_enviado" value="{{ date('Y-m-d') }}" required>
                    <a href="" class="link-input-alterar-data-do-envio" hashId="{{ $certificadoInfos->hash_id }}">
                        <button class="button2">
                            OK
                        </button>
                    </a>
                </div>

                <div class="flash flash-ajax" style="display: none;"></div>
            </div>
            @endif

            @if($certificadoInfos->aviso_vencimento_enviado == 1)
            <div class="card3">
                <div class="thumbs">
                    <img src="{{ asset('assets/img/layout/ico-validado.svg')}}" alt="">
                    <h3>AVISO DE VENCIMENTO</h3>
                </div>
                <div class="flex">
                    <p class="label">AVISO ENVIADO EM: </p>
                    <p class="dados">{{ $certificadoInfos->data_aviso_vencimento_enviado->format('d/m/Y') }}</p>
                </div>
                <a href="" class="link-editar-data-do-envio">
                    <div class="button">
                        EDITAR DATA DO ENVIO
                    </div>
                </a>

                <div class="flex input-data-aviso-de-vencimento" style="display:none;">
                    <meta name="_token" content="{{ csrf_token() }}">
                    <input type="date" name="data_aviso_vencimento_enviado" id="data_aviso_vencimento_enviado" value="{{ date('Y-m-d') }}" required>
                    <a href="" class="link-input-alterar-data-do-envio" hashId="{{ $certificadoInfos->hash_id }}">
                        <button class="button2">
                            OK
                        </button>
                    </a>
                </div>

                <div class="flash flash-ajax" style="display: none;"></div>
                @include('layouts.flash')
            </div>
            @endif
        </div>
    </div>

</main>

@endsection