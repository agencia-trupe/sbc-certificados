@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main>
    <div class="geral">
        <div class="emitidos">
            <h1>ÚLTIMAS TURMAS ABERTAS</h1>
            <hr>
            @foreach($ultimosCertificados as $certificado)
            <div class="line">
                <p>{{ $certificado->data_emissao->format('d/m/Y') }}</p>
                <p>{{ $certificado->curso }}</p>
                <a href="{{ route('admin.certificados.edit', $certificado->hash_id) }}"><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" alt=""></a>
            </div>
            <hr>
            @endforeach
        </div>

        <div class="vencimento">
            <a href="{{ route('admin.relatorios.index') }}" class="link-relatorios">
                <div class="button">
                    RELATÓRIOS
                </div>
            </a>
            <h1>CERTIFICADOS COM VENCIMENTO PRÓXIMO</h1>
            <hr>
            @foreach($certificadosVencimentoProximo as $certificado)
            <div class="line">
                <p class="sentence">{{ $certificado->curso }}</p>
                <p>{{ $certificado->data_validade->format('d/m/Y') }}</p>
                <a href="{{ route('admin.visaoGeral.show', $certificado->hash_id) }}">
                    @if($certificado->aviso_vencimento_enviado == 0)
                    <img src="{{ asset('assets/img/layout/ico-extrair-mailing.svg')}}" alt="">
                    @else
                    <img src="{{ asset('assets/img/layout/ico-validado.svg')}}" alt="">
                    @endif
                </a>
            </div>
            <hr>
            @endforeach
        </div>
    </div>
</main>
@endsection