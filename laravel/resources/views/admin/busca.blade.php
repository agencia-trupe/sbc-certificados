@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="alunos">

    <div class="center2">

        @include('layouts.flash')

        <div class="tabela">
            <div class="cabec">
                <h1>RESULTADOS DA BUSCA</h1>
            </div>

            <div>
                <h4 class="termo">Termo da busca: <span>{{ $termo }}</span></h4>
                <hr>
                @if(count($resultados) > 0)
                @foreach($resultados as $grupos)
                @foreach($grupos as $resultado)
                <a href="{{ $resultado['link'] }}" class="line resultado">
                    <div class="flex2">
                        <p class="name">{{ $resultado['menu'] }}</p>
                        <span>|</span>
                        <p class="reg">{{ $resultado['titulo'] }}</p>
                    </div>
                </a>
                <hr>
                @endforeach
                @endforeach

                @else
                <div class="line">
                    <p class="text">Nenhum registro encontrado.</p>
                </div>
                <hr>
                @endif
            </div>
        </div>
</main>



@endsection