@extends('layouts.template')

@section('content')

<main class="login">

    <div class="externa">
        <div class="imagem">
            <div class="img"></div>
        </div>
        <div class="father">
            <h1>Sistema E-cards SBC</h1>
            <p>• ADMINISTRAÇÃO •</p>
            <div class="logs">
                <form action="{{ route('admin.login.post') }}" method="post">
                    {!! csrf_field() !!}
                    <label for="login">LOGIN (E-MAIL)</label>
                    <input type="text" id="login" name="email">
                    <label for="password">SENHA</label>
                    <input type="password" id="password" name="password">
                    <button type="submit">ENVIAR</button>

                    @include('layouts.flash')

                    <a href="{{ route('admin.esqueci-a-senha') }}">esqueci a senha »</a>
                </form>
            </div>
        </div>
    </div>

    </section>


    @endsection