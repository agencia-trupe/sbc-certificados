@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="alunos">
    <div class="center2">
        <div class="fichaD">

            <div class="cabec">
                <h1>ADMIN | EDITAR CADASTRO</h1>
            </div>

            <div class="card">
                @if(auth()->guard('admin')->check())
                <form action="{{ route('admin.update', $admin->id) }}" method="post" class="wrapper">
                    {!! csrf_field() !!}
                    <div class="campo">
                        <label for="nome" class="label">NOME:</label>
                        <input type="text" id="nome" name="nome" value="{{ $admin->nome }}">
                    </div>
                    <div class="campo">
                        <label for="email" class="label">E-MAIL:</label>
                        <input type="text" id="email" name="email" value="{{ $admin->email }}">
                    </div>
                    <div class="campo">
                        <label for="password" class="label">SENHA:</label>
                        <input type="password" id="password" name="password" value="">
                    </div>
                    <div class="campo">
                        <label for="password_confirmation" class="label">REPETIR SENHA:</label>
                        <input type="password" id="password_confirmation" name="password_confirmation" value="">
                    </div>
                    <button type="submit" class="button_editar_cadastro">ALTERAR</button>

                    @if($errors->any())
                    <div class="flash flash-erro">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                    @endif

                    @if(session('sucesso'))
                    <div class="flash flash-sucesso">
                        <p>Cadastro alterado com sucesso!</p>
                    </div>
                    @endif
                </form>
                @endif
            </div>

        </div>
    </div>
</main>

@endsection