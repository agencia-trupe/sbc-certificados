@extends('layouts.template')

@section('content')

<main class="login">

    <div class="externa">
        <div class="imagem">
            <div class="img"></div>
        </div>
        <div class="father">
            <h1>Sistema E-cards SBC</h1>
            <p>• ADMINISTRAÇÃO •</p>
            <div class="logs">
                <form action="{{ route('admin.esqueci-a-senha.post') }}" method="post">
                    {!! csrf_field() !!}
                    <label for="login">LOGIN (E-MAIL)</label>
                    <input type="text" id="login" name="email" required>
                    <button type="submit">SOLICITAR</button>

                    @include('layouts.flash')

                    <a href="{{ route('admin.login') }}">« voltar</a>
                </form>
            </div>
        </div>
    </div>

    </section>


    @endsection