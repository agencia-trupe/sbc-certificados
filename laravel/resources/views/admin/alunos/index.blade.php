@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="alunos">

    <div class="center2">

        @if(session('repetidos'))
        <div class="flash flash-repetidos">
            @foreach(session('repetidos') as $aluno)
            {!! $aluno !!}<br>
            @endforeach
        </div>
        @endif

        @if(session('erros'))
        <div class="flash flash-erros">
            @foreach(session('erros') as $aluno)
            {!! $aluno !!}<br>
            @endforeach
        </div>
        @endif

        @include('layouts.flash')

        <div class="tabela">
            <div class="cabec">
                <h1>ALUNOS</h1>

                <div class="btns-alunos">
                    <form action="{{ route('admin.alunos.import') }}" method="post" enctype='multipart/form-data'>
                        {!! csrf_field() !!}
                        <button class="btn-upload-excel" id="excel_alunos" type="button">IMPORTAR DADOS</button>
                        <input type="file" name="excel_alunos" style="display: none">
                        <div class="info-importar" style="display: none;">
                            <p class="doc-selecionado"></p>
                            <button type="submit" class="btn-importar">ENVIAR</button>
                        </div>
                    </form>
                    <a href="{{ route('admin.alunos.create') }}">
                        <div class="cadastro">
                            NOVO CADASTRO
                        </div>
                    </a>
                </div>
            </div>

            <div>
                <div class="line">
                    <div class="flex2">
                        <p class="name header">ID ALUNO</p>
                        <span>|</span>
                        <p class="name cpf header">CPF</p>
                        <span>|</span>
                        <p class="name header">Nome completo</p>
                    </div>
                    <div class="flex">
                        <p class="reg header">ID Certificado</p>
                        <span>|</span>
                        <p class="text header">Curso</p>
                    </div>
                    <div class="botoes">
                    </div>
                </div>
                <hr>
                @if(count($alunos) > 0)
                @foreach($alunos as $aluno)
                <div class="line">
                    <div class="flex2">
                        <p class="name id-aluno">{{ $aluno->hash_id }}</p>
                        <span>|</span>
                        <p class="name cpf">{{ $aluno->cpf }}</p>
                        <span>|</span>
                        <p class="name">{{ $aluno->nome }}</p>
                    </div>
                    <div class="flex">
                        @foreach($certificados as $certificado)
                        @if($certificado->aluno_id == $aluno->id)
                        <p class="reg">{{ $certificado->hash_aluno_certif }}</p>
                        <span>|</span>
                        <p class="text">{{ $certificado->curso }}</p>
                        @endif
                        @endforeach
                    </div>
                    <div class="botoes">
                        <div class="btn-show">
                            @if(count($certificados->where('aluno_id', $aluno->id)) > 0)
                            <a href="{{ route('admin.alunos.show', $aluno->hash_id) }}"><img src="{{ asset('assets/img/layout/ico-cadastroaluno.svg')}}" class="editar" alt=""></a>
                            @endif
                        </div>
                        <div class="btn-edit">
                            <a href="{{ route('admin.alunos.edit', $aluno->hash_id) }}"><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" class="editar" alt=""></a>
                        </div>
                        <div class="btn-delete">
                            @if(count($certificados->where('aluno_id', $aluno->id)) == 0)
                            <a href="{{ route('admin.alunos.delete', $aluno->hash_id) }}" onclick="return confirm('Tem certeza que deseja excluir o aluno selecionado?')"><img src="{{ asset('assets/img/layout/x-sair.svg')}}" class="sair" alt=""></a>
                            @endif
                        </div>
                    </div>
                </div>
                <hr>
                @endforeach

                @else
                <div class="line">
                    <p class="text">Nenhum registro encontrado.</p>
                </div>
                <hr>
                @endif
            </div>

            <div class="pages">
                {!! $alunos->links() !!}
            </div>
        </div>
</main>



@endsection