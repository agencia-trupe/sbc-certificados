@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="alunos">

    <div class="center2">

        <div class="fichaD">

            <div class="cabec">
                @if(isset($aluno))
                <h1>ALUNOS | EDITAR CADASTRO</h1>
                @else
                <h1>ALUNOS | NOVO CADASTRO</h1>
                @endif
            </div>

            <div class="card">
                @if(isset($aluno))
                <form action="{{ route('admin.alunos.update', $aluno->hash_id) }}" method="post" class="wrapper">
                    @else
                    <form action="{{ route('admin.alunos.store') }}" method="post" class="wrapper">
                        @endif
                        {!! csrf_field() !!}
                        <div class="campo">
                            <label for="cpf" class="label">CPF:</label>
                            <input type="text" id="cpf" name="cpf" class="input-cpf" value="{{ isset($aluno) ? $aluno->cpf : '' }}">
                        </div>
                        <div class="campo">
                            <label for="nome" class="label">Nome Completo:</label>
                            <input type="text" id="nome" name="nome" value="{{ isset($aluno) ? $aluno->nome : '' }}">
                        </div>
                        <div class="campo">
                            <label for="email" class="label">E-mail:</label>
                            <input type="text" id="email" name="email" value="{{ isset($aluno) ? $aluno->email : '' }}">
                        </div>
                        <div class="campo">
                            <label for="profissao" class="label">Profissão:</label>
                            <input type="text" id="profissao" name="profissao" value="{{ isset($aluno) ? $aluno->profissao : '' }}">
                        </div>
                        <div class="botoes">
                            @if(isset($aluno))
                            <button type="submit" class="button_insere">ALTERAR</button>
                            @else
                            <button type="submit" class="button_insere">INSERIR</button>
                            @endif
                            <a href="{{ route('admin.alunos.index') }}" class="button_voltar">CANCELAR</a>
                        </div>

                        @include('layouts.flash')
                    </form>
            </div>
        </div>
    </div>
</main>

@endsection