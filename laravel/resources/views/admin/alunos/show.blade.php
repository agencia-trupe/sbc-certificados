@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="alunos">

    <div class="center2">

        @include('layouts.flash')

        <div class="fichaD2">
            <div class="cabec">
                <h1>ALUNOS | VISUALIZAR DETALHES</h1>
            </div>

            @foreach($certificados as $certificado)
            @if($certificado->data_validade > date('Y-m-d'))
            <div class="certificado-ativo">
                <div class="print">
                    <div class="certified">
                        <div class="detail_blue"></div>
                        <div class="imgs">
                            <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                            <img src="{{ asset('assets/img/cursos/'.$certificado->curso_imagem) }}">
                        </div>
                        <div class="infos">
                            <p class="nome">{{ $certificado->aluno_nome }}</p>
                            <p class="subtrace">concluiu com êxito o curso, currículo e as avaliações propostas para o treinamento:</p>
                            <p class="curso">{{ $certificado->curso_titulo }}</p>
                        </div>
                        <div class="cert_rodape">
                            <div class="QRCODE">
                                <img src="{{ asset('assets/img/qr-code/'.$certificado->qr_code) }}">
                            </div>
                            <div class="veracidade">
                                <div class="val">
                                    <div class="box_1">
                                        <p class="label">DATA DE EMISSÃO:</p>
                                        <p class="date">{{ strftime("%d %h %Y", strtotime($certificado->data_emissao)) }}</p>
                                    </div>
                                    <div class="box_2">
                                        <p class="label">VALIDO ATÉ:</p>
                                        <p class="date">{{ strftime("%d %h %Y", strtotime($certificado->data_validade)) }}</p>
                                    </div>
                                    <div class="box_2">
                                        <p class="label">CÓDIGO DESTE CERTIFICADO:</p>
                                        <p class="date">{{ $certificado->ac_hash_id }}</p>
                                    </div>
                                </div>
                                <p class="consulte">Consulte a veracidade e validade deste documento lendo este QR Code, ou acesse: <a href="https://ecards.cardiol.br">https://ecards.cardiol.br</a> e informe o código do certificado.</p>

                            </div>
                        </div>
                    </div>

                    <div class="certified">
                        <div class="detail_blue"></div>
                        <div class="imgs">
                            <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                            <img src="{{ asset('assets/img/cursos/'.$certificado->curso_imagem) }}">
                        </div>
                        <div class="body">
                            <div class="release">
                                <p>© {{ date('Y') }} Sociedade Brasileira de Cardiologia</p>
                            </div>
                            <div class="table">
                                <div class="box_3">
                                    <P class="label">CURSO</P>
                                    <p class="text">{{ $certificado->curso_titulo }}</p>
                                </div>
                                <div class="box_3">
                                    <P class="label">DIRETOR DO CURSO</P>
                                    <p class="text">{{ $certificado->instrutor_id }} | {{ $certificado->instrutor_nome }}</p>
                                </div>
                                <div class="box_3">
                                    <P class="label">CENTRO DE TREINAMENTO</P>
                                    <p class="text">{{ $certificado->instituicao_nome }}</p>
                                </div>
                                <div class="box_3">
                                    <P class="label">LOCAL</P>
                                    <p class="text">{{ $certificado->cidade }}, {{ $certificado->uf }}</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="buttons">
                    <a href="" class="link-reenviar-certificado-aluno" certificadoId="{{ $certificado->hash_id }}" alunoId="{{ $aluno->hash_id }}">
                        <div class="red_button">
                            REENVIAR CERTIFICADO <img src="{{ asset('assets/img/layout/ico-enviar-email.svg')}}" alt="">
                        </div>
                    </a>
                </div>

            </div>
            @else
            <div class="certificado-vencido">
                <div class="print">
                    <div class="certified" style="background-image: url({{ asset('assets/img/layout/bg-vencido.svg') }})">
                        <div class="detail_vinho"></div>
                        <div class="imgs">
                            <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                            <img src="{{ asset('assets/img/cursos/'.$certificado->curso_imagem) }}">
                        </div>
                        <div class="infos">
                            <p class="nome">{{ $certificado->aluno_nome }}</p>
                            <p class="subtrace">concluiu com êxito o curso, currículo e as avaliações propostas para o treinamento:</p>
                            <p class="curso">{{ $certificado->curso_titulo }}</p>
                        </div>
                        <div class="cert_rodape">
                            <div class="QRCODE">
                                <img src="{{ asset('assets/img/qr-code/'.$certificado->qr_code) }}">
                            </div>
                            <div class="veracidade">
                                <div class="val">
                                    <div class="box_1">
                                        <p class="label">DATA DE EMISSÃO:</p>
                                        <p class="date">{{ strftime("%d %h %Y", strtotime($certificado->data_emissao)) }}</p>
                                    </div>
                                    <div class="box_2">
                                        <p class="label">VALIDO ATÉ:</p>
                                        <p class="date">{{ strftime("%d %h %Y", strtotime($certificado->data_validade)) }}</p>
                                    </div>
                                    <div class="box_2">
                                        <p class="label">CÓDIGO DESTE CERTIFICADO:</p>
                                        <p class="date">{{ $certificado->ac_hash_id }}</p>
                                    </div>
                                </div>
                                <p class="consulte">Consulte a veracidade e validade deste documento lendo este QR Code, ou acesse: <a href="https://ecards.cardiol.br">https://ecards.cardiol.br</a> e informe o código do certificado.</p>

                            </div>
                        </div>
                    </div>

                    <div class="certified" style="background-image: url({{ asset('assets/img/layout/bg-vencido.svg') }})">
                        <div class="detail_vinho"></div>
                        <div class="imgs">
                            <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                            <img src="{{ asset('assets/img/cursos/'.$certificado->curso_imagem) }}">
                        </div>
                        <div class="body">
                            <div class="release">
                                <p>© {{ date('Y') }} Sociedade Brasileira de Cardiologia</p>
                            </div>
                            <div class="table">
                                <div class="box_3">
                                    <P class="label">CURSO</P>
                                    <p class="text">{{ $certificado->curso_titulo }}</p>
                                </div>
                                <div class="box_3">
                                    <P class="label">DIRETOR DO CURSO</P>
                                    <p class="text">{{ $certificado->instrutor_id }} | {{ $certificado->instrutor_nome }}</p>
                                </div>
                                <div class="box_3">
                                    <P class="label">CENTRO DE TREINAMENTO</P>
                                    <p class="text">{{ $certificado->instituicao_nome }}</p>
                                </div>
                                <div class="box_3">
                                    <P class="label">LOCAL</P>
                                    <p class="text">{{ $certificado->cidade }}, {{ $certificado->uf }}</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @endif
            @endforeach

            <div class="flash flash-ajax" style="display: none;"></div>

        </div>
        <div class="wrapper2">
            <a href="{{ route('admin.alunos.index') }}">
                <div class="button_voltar">
                    VOLTAR
                </div>
            </a>
        </div>
</main>



@endsection