@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="instrutores">

    <div class="center">

        <div class="ficha">
            <div class="cabec">
                @if(isset($instituicao))
                <h1>INSTITUIÇÕES | EDITAR CADASTRO</h1>
                @else
                <h1>INSTITUIÇÕES | NOVO CADASTRO</h1>
                @endif
            </div>

            <div class="card">
                @if(isset($instituicao))
                <form action="{{ route('admin.instituicoes.update', $instituicao->hash_id) }}" method="post" class="wrapper">
                    @else
                    <form action="{{ route('admin.instituicoes.store') }}" method="post" class="wrapper">
                        @endif
                        {!! csrf_field() !!}
                        <div class="campo">
                            <label for="nome" class="label">Nome da Instituição:</label>
                            <input type="text" id="nome" name="nome" value="{{ isset($instituicao) ? $instituicao->nome : '' }}">
                        </div>
                        <div class="botoes">
                            @if(isset($instituicao))
                            <button type="submit" class="button_insere">ALTERAR</button>
                            @else
                            <button type="submit" class="button_insere">INSERIR</button>
                            @endif
                            <a href="{{ route('admin.instituicoes.index') }}" class="button_voltar">CANCELAR</a>
                        </div>

                        @include('layouts.flash')
                    </form>
            </div>
        </div>
    </div>

</main>

@endsection