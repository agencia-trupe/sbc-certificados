@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="instrutores">

    <div class="center">

        @include('layouts.flash')

        <div class="tabela">
            <div class="cabec">
                <h1>INSTITUIÇÕES</h1>
                <a href="{{ route('admin.instituicoes.create') }}">
                    <div class="cadastro">
                        NOVO CADASTRO
                    </div>
                </a>
            </div>
            <div>
                <hr>
                @if(count($instituicoes) > 0)
                @foreach($instituicoes as $instituicao)
                <div class="line">
                    <div class="flex">
                        <p class="name">{{ $instituicao->nome }}</p>
                    </div>
                    <div class="botoes">
                        <div class="btn-edit">
                            <a href="{{ route('admin.instituicoes.edit', $instituicao->hash_id) }}"><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" class="editar" alt=""></a>
                        </div>
                        <div class="btn-delete">
                            @if(count($certificados->where('instituicao_id', $instituicao->id)) == 0)
                            <a href="{{ route('admin.instituicoes.delete', $instituicao->hash_id) }}" onclick="return confirm('Tem certeza que deseja excluir a instituição selecionada?')"><img src="{{ asset('assets/img/layout/x-sair.svg')}}" class="sair" alt=""></a>
                            @endif
                        </div>
                    </div>
                </div>
                <hr>
                @endforeach

                @else
                <div class="line">
                    <p class="text">Nenhum registro encontrado.</p>
                </div>
                <hr>
                @endif
            </div>
        </div>

        <div class="pages">
            {!! $instituicoes->links() !!}
        </div>

    </div>
</main>

@endsection