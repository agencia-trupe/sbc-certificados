@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="cursos">

    <div class="center">

        @include('layouts.flash')

        <div class="tabela">
            <div class="cabec">
                <h1>CURSOS</h1>
                <a href="{{ route('admin.cursos.create') }}">
                    <div class="cadastro">
                        NOVO CADASTRO
                    </div>
                </a>
            </div>
            <div>
                <hr>

                @if(count($cursos) > 0)
                @foreach($cursos as $curso)
                <div class="line">
                    <div class="flex">
                        <div class="img-curso" style="width: 215px; height: 45px;">
                            @if(isset($curso->imagem))
                            <img src="{{ asset('assets/img/cursos/'.$curso->imagem) }}" width="215" height="45" alt="">
                            @else
                            <img src="{{ asset('assets/img/layout/sem-imagem.png') }}" height="45" alt="">
                            @endif
                        </div>
                        <p class="name">{{ $curso->titulo }}</p>
                    </div>
                    <div class="botoes">
                        <div class="btn-edit">
                            <a href="{{ route('admin.cursos.edit', $curso->hash_id) }}"><img src="{{ asset('assets/img/layout/ico-editar.svg')}}" class="editar" alt=""></a>
                        </div>
                        <div class="btn-delete">
                            @if(count($certificados->where('curso_id', $curso->id)) == 0)
                            <a href="{{ route('admin.cursos.delete', $curso->hash_id) }}" onclick="return confirm('Tem certeza que deseja excluir o curso selecionado?')"><img src="{{ asset('assets/img/layout/x-sair.svg')}}" class="sair" alt=""></a>
                            @endif
                        </div>
                    </div>
                </div>
                <hr>
                @endforeach

                @else
                <div class="line">
                    <p class="text">Nenhum registro encontrado.</p>
                </div>
                <hr>
                @endif
            </div>
        </div>

        <div class="pages">
            {!! $cursos->links() !!}
        </div>

    </div>
</main>

@endsection