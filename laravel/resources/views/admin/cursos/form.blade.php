@extends('layouts.template')

@section('header')
@include('layouts.header')
@endsection

@section('content')

<main class="cursos">

    <div class="center">

        <div class="fichab">
            <div class="cabec">
                @if(isset($curso))
                <h1>CURSOS | EDITAR CADASTRO</h1>
                @else
                <h1>CURSOS | NOVO CADASTRO</h1>
                @endif
            </div>
            <div class="card">
                @if(isset($curso))
                <form action="{{ route('admin.cursos.update', $curso->hash_id) }}" method="post" class="wrapper" enctype="multipart/form-data">
                    @else
                    <form action="{{ route('admin.cursos.store') }}" method="post" class="wrapper" enctype="multipart/form-data">
                        @endif
                        {!! csrf_field() !!}
                        <div class="campo">
                            <label for="titulo" class="label">Título do Curso:</label>
                            <input type="text" id="titulo" name="titulo" value="{{ isset($curso) ? $curso->titulo : '' }}">
                        </div>
                        <div class="campo">
                            <label for="imagem" class="label">Imagem do Curso (tamanho 430 x 90 pixels):</label>
                            @if(isset($curso) && isset($curso->imagem))
                            <img src="{{ asset('assets/img/cursos/'.$curso->imagem) }}" alt="">
                            @else
                            <img src="{{ asset('assets/img/layout/sem-imagem.png') }}" width="200" alt="">
                            @endif
                            <input type="text" placeholder="(selecionar imagem)" disabled>
                            <button class="btn-upload-imagem-curso" id="imagem" type="button">CARREGAR IMAGEM</button>
                            <input type="file" name="imagem" style="display: none">
                        </div>

                        <div class="botoes">
                            @if(isset($curso))
                            <button type="submit" class="button_insere">ALTERAR</button>
                            @else
                            <button type="submit" class="button_insere">INSERIR</button>
                            @endif
                            <a href="{{ route('admin.cursos.index') }}" class="button_voltar">CANCELAR</a>
                        </div>

                        @include('layouts.flash')
                    </form>
            </div>
        </div>
    </div>

    </div>
</main>

@endsection