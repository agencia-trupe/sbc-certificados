@extends('layouts.template')

@section('content')

<main class="login">

    <div class="externa">
        <div class="imagem">
            <div class="img"></div>
        </div>
        <div class="father">
            <h1>Sistema E-cards SBC</h1>
            <p>• POLÍTICA DE PRIVACIDADE •</p>

            <div class="politica">
                {!! $politica->texto !!}
                <!-- texto tipo blog, colocar css pra todos os itens dentro (p, a, img, table) -->
            </div>
        </div>
    </div>

</main>

@endsection