@extends('layouts.template')

@section('content')

<main class="login">

    <div class="externa">
        <div class="imagem">
            <div class="img"></div>
        </div>
        <div class="father">
            <h1>Sistema E-cards SBC</h1>
            <p>• CONSULTA DE CERTIFICADOS •</p>

            @if($certificado == null)
            <div class="not_found">
                <div class="msg">
                    <h1>O CÓDIGO DO CERTIFICADO INFORMADO NÃO EXISTE. FAÇA UMA NOVA CONSULTA.</h1>
                </div>
                <a href="{{ route('consulta.index') }}">
                    <div class="button_voltar">
                        VOLTAR
                    </div>
                </a>
            </div>

            @else

            @if($certificado->data_validade > date('Y-m-d'))
            <div class="show">
                <div class="resumo">
                    <h1>O CERTIFICADO ABAIXO ESTÁ ATIVO</h1>
                    <p>na data de hoje: {{ date('d/m/Y') }}</p>
                </div>

                <div class="certified">
                    <div class="detail_blue"></div>
                    <div class="imgs">
                        <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                        <img src="{{ asset('assets/img/cursos/'.$certificado->curso_imagem) }}">
                    </div>
                    <div class="infos">
                        <p class="nome">{{ $certificado->aluno_nome }}</p>
                        <p class="subtrace">concluiu com êxito o curso, currículo e as avaliações propostas para o treinamento:</p>
                        <p class="curso">{{ $certificado->curso_titulo }}</p>
                    </div>
                    <div class="cert_rodape">
                        <div class="QRCODE">
                            <img src="{{ asset('assets/img/qr-code/'.$certificado->qr_code) }}">
                        </div>
                        <div class="veracidade">
                            <div class="val">
                                <div class="box_1">
                                    <p class="label">DATA DE EMISSÃO:</p>
                                    <p class="date">{{ strftime("%d %h %Y", strtotime($certificado->data_emissao)) }}</p>
                                </div>
                                <div class="box_2">
                                    <p class="label">VALIDO ATÉ:</p>
                                    <p class="date">{{ strftime("%d %h %Y", strtotime($certificado->data_validade)) }}</p>
                                </div>
                                <div class="box_2">
                                    <p class="label">CÓDIGO DESTE CERTIFICADO:</p>
                                    <p class="date">{{ $certificado->ac_hash_id }}</p>
                                </div>
                            </div>
                            <p class="consulte">Consulte a veracidade e validade deste documento lendo este QR Code, ou acesse: <a href="https://ecards.cardiol.br">https://ecards.cardiol.br</a> e informe o código do certificado.</p>

                        </div>
                    </div>
                </div>

                <div class="certified">
                    <div class="detail_blue"></div>
                    <div class="imgs">
                        <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                        <img src="{{ asset('assets/img/cursos/'.$certificado->curso_imagem) }}">
                    </div>
                    <div class="body">
                        <div class="release">
                            <p>© {{ date('Y') }} Sociedade Brasileira de Cardiologia</p>
                        </div>
                        <div class="table">
                            <div class="box_3">
                                <P class="label">CURSO</P>
                                <p class="text">{{ $certificado->curso_titulo }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">DIRETOR DO CURSO</P>
                                <p class="text">{{ $certificado->instrutor_id }} | {{ $certificado->instrutor_nome }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">CENTRO DE TREINAMENTO</P>
                                <p class="text">{{ $certificado->instituicao_nome }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">LOCAL</P>
                                <p class="text">{{ $certificado->cidade }}, {{ $certificado->uf }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="buttons">
                    <a href="{{ route('consulta.showPrint', $certificado->ac_hash_id) }}" target="_blank">
                        <div class="print">
                            IMPRIMIR
                        </div>
                    </a>

                    <a href="{{ route('consulta.index') }}">
                        <div class="button_voltar">
                            VOLTAR
                        </div>
                    </a>
                </div>
            </div>
            @endif

            @if($certificado->data_validade <= date('Y-m-d')) <div class="show">
                <div class="resumo bg-vinho">
                    <h1>O CERTIFICADO ABAIXO ESTÁ VENCIDO</h1>
                    <p>na data de hoje: {{ date('d/m/Y') }}</p>
                </div>

                <div class="certified">
                    <div class="bg-vencido"></div>
                    <div class="detail_vinho"></div>
                    <div class="imgs">
                        <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                        <img src="{{ asset('assets/img/cursos/'.$certificado->curso_imagem) }}">
                    </div>
                    <div class="infos">
                        <p class="nome">{{ $certificado->aluno_nome }}</p>
                        <p class="subtrace">concluiu com êxito o curso, currículo e as avaliações propostas para o treinamento:</p>
                        <p class="curso">{{ $certificado->curso_titulo }}</p>
                    </div>
                    <div class="cert_rodape">
                        <div class="QRCODE">
                            <img src="{{ asset('assets/img/qr-code/'.$certificado->qr_code) }}">
                        </div>
                        <div class="veracidade">
                            <div class="val">
                                <div class="box_1">
                                    <p class="label">DATA DE EMISSÃO:</p>
                                    <p class="date">{{ strftime("%d %h %Y", strtotime($certificado->data_emissao)) }}</p>
                                </div>
                                <div class="box_2">
                                    <p class="label">VALIDO ATÉ:</p>
                                    <p class="date">{{ strftime("%d %h %Y", strtotime($certificado->data_validade)) }}</p>
                                </div>
                                <div class="box_2">
                                    <p class="label">CÓDIGO DESTE CERTIFICADO:</p>
                                    <p class="date">{{ $certificado->ac_hash_id }}</p>
                                </div>
                            </div>
                            <p class="consulte">Consulte a veracidade e validade deste documento lendo este QR Code, ou acesse: <a href="https://ecards.cardiol.br">https://ecards.cardiol.br</a> e informe o código do certificado.</p>

                        </div>
                    </div>
                </div>

                <div class="certified">
                    <div class="bg-vencido"></div>
                    <div class="detail_vinho"></div>
                    <div class="imgs">
                        <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                        <img src="{{ asset('assets/img/cursos/'.$certificado->curso_imagem) }}">
                    </div>
                    <div class="body">
                        <div class="release">
                            <p>© {{ date('Y') }} Sociedade Brasileira de Cardiologia</p>
                        </div>
                        <div class="table">
                            <div class="box_3">
                                <P class="label">CURSO</P>
                                <p class="text">{{ $certificado->curso_titulo }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">DIRETOR DO CURSO</P>
                                <p class="text">{{ $certificado->instrutor_id }} | {{ $certificado->instrutor_nome }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">CENTRO DE TREINAMENTO</P>
                                <p class="text">{{ $certificado->instituicao_nome }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">LOCAL</P>
                                <p class="text">{{ $certificado->cidade }}, {{ $certificado->uf }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="buttons">
                    <a href="{{ route('consulta.showPrint', $certificado->ac_hash_id) }}" target="_blank">
                        <div class="print">
                            IMPRIMIR
                        </div>
                    </a>

                    <a href="{{ route('consulta.index') }}">
                        <div class="button_voltar">
                            VOLTAR
                        </div>
                    </a>
                </div>
        </div>
        @endif
        @endif
    </div>
    </div>

</main>

@endsection