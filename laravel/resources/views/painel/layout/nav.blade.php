<ul class="nav navbar-nav">
    <li>
        <a href="{{ route('painel.admins.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.admins*')) active @endif">Usuários Área Admin</a>
    </li>
    <li>
        <a href="{{ route('painel.area-admin.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.area-admin*')) active @endif">Dados Área Admin</a>
    </li>
    <li>
        <a href="{{ route('painel.contatos.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.contatos.index')) active @endif">Informações de contato</a>
    </li>
    <li>
        <a href="{{ route('painel.politica-de-privacidade.index') }}" class="nav-link px-3 @if(Tools::routeIs('painel.politica-de-privacidade*')) active @endif">Política de Privacidade</a>
    </li>
</ul>