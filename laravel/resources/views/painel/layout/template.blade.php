<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <title>{{ config('app.name') }} - Painel Administrativo</title>

    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/vendor.painel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/painel.css') }}">

</head>

<body>
    @include('painel.layout.header')

    <div class="container mt-5" style="padding-bottom:30px;">
        @yield('content')
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-mask-plugin-master/dist/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/ckeditor5-custom-online/build/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/js/vendor.painel.js') }}"></script>
    <script src="{{ asset('assets/js/painel.js') }}"></script>

</body>

</html>