@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('email', 'E-mail (para recebimento/envio de contatos)') !!}
    {!! Form::text('email', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}
</div>