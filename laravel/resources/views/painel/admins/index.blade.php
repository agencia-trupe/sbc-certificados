@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">ÁREA ADMIN - Usuários</h2>
    <a href="{{ route('painel.admins.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Admin
    </a>
</legend>

<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">E-mail</th>
                <th scope="col"><i class="bi bi-gear-fill me-2"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($admins as $admin)
            <tr>
                <td>{{ $admin->nome }}</td>
                <td>{{ $admin->email }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.admins.destroy', $admin->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.admins.edit', $admin->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>

            @endforeach
        </tbody>
    </table>
</div>

@endsection