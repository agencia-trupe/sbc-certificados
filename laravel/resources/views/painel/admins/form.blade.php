@include('painel.layout.flash')

<div class="mb-3">
    {!! Form::label('nome', 'Usuário Admin', ['class' => 'form-label']) !!}
    {!! Form::text('nome', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3">
    {!! Form::label('email', 'E-mail', ['class' => 'form-label']) !!}
    {!! Form::email('email', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3">
    {!! Form::label('password', 'Senha', ['class' => 'form-label']) !!}
    {!! Form::password('password', ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3">
    {!! Form::label('password_confirmation', 'Confirmação de Senha', ['class' => 'form-label']) !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('painel.admins.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>