@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">Editar Admin</h2>
</legend>

{!! Form::model($admin, [
'route' => ['painel.admins.update', $admin->id],
'method' => 'patch'])
!!}

@include('painel.admins.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection