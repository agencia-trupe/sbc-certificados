@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">Adicionar Admin</h2>
</legend>

{!! Form::open(['route' => 'painel.admins.store']) !!}

@include('painel.admins.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection