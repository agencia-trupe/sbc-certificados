@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">ÁREA ADMIN - Dados</h2>
</legend>

{!! Form::model($area_admin, [
'route' => ['painel.area-admin.update', $area_admin->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.area-admin.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection