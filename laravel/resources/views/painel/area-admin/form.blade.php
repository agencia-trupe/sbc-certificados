@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('tempo_aviso_vencimento', 'Tempo para aviso de vencimento dos certificados (Visão Geral)') !!}
    {!! Form::number('tempo_aviso_vencimento', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_email_vencimento', 'Texto dos e-mails sobre o vencimento dos certificados') !!}
    {!! Form::textarea('texto_email_vencimento', null, ['class' => 'form-control editor-basic']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_email_certificado', 'Texto dos e-mails para envio dos certificados') !!}
    {!! Form::textarea('texto_email_certificado', null, ['class' => 'form-control editor-basic']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}
</div>