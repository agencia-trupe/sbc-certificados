@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">Editar Usuário</h2>
</legend>

{!! Form::model($usuario, [
'route' => ['painel.usuarios.update', $usuario->id],
'method' => 'patch'])
!!}

@include('painel.usuarios.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection