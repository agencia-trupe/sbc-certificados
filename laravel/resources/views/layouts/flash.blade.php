@if($errors->any())
<div class="flash flash-erro">
    @foreach($errors->all() as $error)
    {!! $error !!}<br>
    @endforeach
</div>
@endif

@if(session('success'))
<div class="flash flash-sucesso">
    {!! session('success') !!}
</div>
@endif