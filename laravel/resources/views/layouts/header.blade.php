<header>
    <div class="external">
        <div class="ajuste">
            <div class="bege"></div>
            <div class="imagem">
                <div class="img"></div>
            </div>
        </div>
        <div class="superior">
            <div class="info">
                <h1>Sistema E-cards SBC</h1>
                <div class="account">
                    <div class="flex">
                        <p>Olá {{ auth('admin')->user()->nome }}!
                            <a href="{{ route('admin.edit', auth('admin')->user()->id) }}">[CONFIGURAÇÕES]</a>
                        </p>
                        <form action="{{ route('admin.logout') }}" method="post" class="wrapper">
                            {!! csrf_field() !!}
                            <button type="submit">SAIR<img src="{{ asset('assets/img/layout/x-sair.svg')}}" alt=""></button>
                        </form>
                    </div>
                    <form action="{{ route('admin.search') }}" method="post" class="search">
                        {!! csrf_field() !!}
                        <input type="text" name="busca" placeholder="buscar">
                        <button type="submit">
                            <img src="{{ asset('assets/img/layout/ico-lupa.svg')}}" alt="">
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="inferior">
            <nav>
                <a href="{{ route('admin.visaoGeral.index') }}">
                    <div class="menu @if(Tools::routeIs('admin.visaoGeral*')) active @endif">
                        VISÃO GERAL
                    </div>
                </a>
                <a href="{{ route('admin.certificados.index') }}">
                    <div class="menu @if(Tools::routeIs('admin.certificados*')) active @endif">
                        CERTIFICADOS/TURMAS
                    </div>
                </a>
                <a href="{{ route('admin.instrutores.index') }}">
                    <div class="menu @if(Tools::routeIs('admin.instrutores*')) active @endif">
                        INSTRUTORES
                    </div>
                </a>
                <a href="{{ route('admin.instituicoes.index') }}">
                    <div class="menu @if(Tools::routeIs('admin.instituicoes*')) active @endif">
                        INSTITUIÇÕES
                    </div>
                </a>
                <a href="{{ route('admin.cursos.index') }}">
                    <div class="menu @if(Tools::routeIs('admin.cursos*')) active @endif">
                        CURSOS
                    </div>
                </a>
                <a href="{{ route('admin.alunos.index') }}">
                    <div class="menu @if(Tools::routeIs('admin.alunos*')) active @endif">
                        ALUNOS
                    </div>
                </a>
            </nav>
        </div>

        <nav>

        </nav>

    </div>

</header>