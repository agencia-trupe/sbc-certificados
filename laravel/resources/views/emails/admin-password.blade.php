<!DOCTYPE html>
<html>

<head>
    <title>[RECUPERAÇÃO DE SENHA] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>

<body>
    <p style='color:#000;font-size:14px;font-family:Verdana;'>
        Você solicitou recuperação da sua senha em nosso sistema. As senhas são criptografadas e seguras, portanto você deve criar uma nova senha acessando o link:<br>
        <a href="{{ route('admin.redefinir-senha', $token).'?email='.urlencode($email) }}" style='font-weight:bold;font-size:16px;font-family:Verdana;'>Clique aqui para redefinir sua senha.</a>
    </p>
</body>

</html>