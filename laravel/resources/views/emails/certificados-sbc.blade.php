<!DOCTYPE html>
<html>

<head>
    <title>[CERTIFICADO | Curso: {{ $aluno->curso }}] - {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>

<body>
    <p style='color:#000;font-size:14px;font-family:Verdana;'>
        {!! $textoEmail !!}<br>
        <a href="{{ route('profissionais.login') }}" style='font-weight:bold;font-size:16px;font-family:Verdana;'>Clique aqui e acesse os certificados.</a>
    </p>
</body>

</html>