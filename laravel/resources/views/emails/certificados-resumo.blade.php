<!DOCTYPE html>
<html>

<head>
    <title>[CERTIFICADO | Resumo Certificado Emitido] - {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>

<body>
    <p style='color:#000;font-size:14px;font-family:Verdana;'>Um certificado foi emitido.</p>
    <br>
    <p style='color:#000;font-size:14px;font-family:Verdana;'>Certificado enviado em: <strong>{{ $data }}</strong></p>
    <table>
        <thead>
            <tr>
                <td style='color:#000;font-size:12px;font-family:Verdana;'>CPF</td>
                <td style='color:#000;font-size:12px;font-family:Verdana;'>Nome</td>
                <td style='color:#000;font-size:12px;font-family:Verdana;'>e-mail</td>
                <td style='color:#000;font-size:12px;font-family:Verdana;'>Número do certificado</td>
            </tr>
        </thead>
        <tbody>
            @foreach($alunos as $aluno)
            <tr>
                <td style='color:#000;font-size:12px;font-family:Verdana;'>{{ $aluno->cpf }}</td>
                <td style='color:#000;font-size:12px;font-family:Verdana;'>{{ $aluno->nome }}</td>
                <td style='color:#000;font-size:12px;font-family:Verdana;'>{{ $aluno->email }}</td>
                <td style='color:#000;font-size:12px;font-family:Verdana;'>{{ $aluno->ac_hash_id }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <a href="{{ route('admin.certificados.index') }}" style='font-weight:bold;font-size:16px;font-family:Verdana;'>Clique aqui e acesse os certificados.</a>
</body>

</html>