<!doctype html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{ url('/') }}">

    <title>SBC Certificados</title>

    <style type="text/css">
        #certificadoPrint {
            width: 680px;
            margin: 0 30px;
            background-color: #FFF;
        }

        .father {
            width: 100% !important;
        }

        .show {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .certified {
            width: 50% !important;
            height: 225px !important;
            margin-bottom: 20px !important;
            background-color: #FFF;
            border: 1px solid lightgray;
        }

        .detail_blue {
            background-color: #1C325D;
        }

        .detail_vinho {
            background-color: #690000;
        }

        .detail_blue,
        .detail_vinho {
            height: 10px;
            width: 100% !important;
        }

        .imgs {
            height: 50px !important;
            margin-top: 10px !important;
            margin-left: auto;
            margin-right: auto;
            display: flex;
            column-gap: 20px;
        }

        .infos {
            margin-left: 90px !important;
            margin-bottom: 10px !important;
            margin-top: 10px;
        }

        .nome {
            width: 92% !important;
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 11px !important;
            font-weight: 700;
            margin-top: 0;
            margin-bottom: 0;
            text-transform: uppercase;
        }

        .subtrace {
            width: 92% !important;
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 8px;
            margin-top: 0;
            margin-bottom: 10px;
        }

        .curso {
            width: 92% !important;
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 11px;
            font-weight: 700;
            text-transform: uppercase;
            margin-top: 0;
            margin-bottom: 0;
        }

        .cert_rodape {
            width: 100% !important;
            height: 60px;
            display: flex;
            margin-left: auto;
            margin-right: auto;
            margin-bottom: 10px;
            column-gap: 20px;
        }

        .QRCODE {
            margin: 0 0 0 15px;
            height: 60px;
            width: 16%;
        }

        .QRCODE img {
            width: 60px;
            height: 60px;
        }

        .veracidade {
            width: 74% !important;
        }

        .veracidade .val {
            width: 92% !important;
            display: flex;
        }

        .veracidade .val .box_1 {
            width: 29% !important;
            border: solid 1px lightgrey;
            padding: 5px;
            padding-left: 10px;
        }

        .veracidade .val .box_1 .label {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 5px;
            margin-top: 0;
            margin-bottom: 5px;
        }

        .veracidade .val .box_1 .date {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 8px;
            margin-top: 0;
            margin-bottom: 0;
            text-transform: uppercase;
        }

        .veracidade .val .box_2 {
            width: 29% !important;
            border: solid 1px lightgrey;
            border-left: 0;
            padding: 5px;
            padding-left: 10px;
        }

        .veracidade .val .box_2.codigo {
            width: 42% !important;
        }

        .veracidade .val .box_2 .label {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 5px;
            margin-top: 0;
            margin-bottom: 5px;
        }

        .veracidade .val .box_2 .date {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 8px;
            margin-top: 0;
            margin-bottom: 0;
            text-transform: uppercase;
        }

        .consulte {
            width: 100% !important;
            margin-top: 15px !important;
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 7px;
        }

        .body {
            column-gap: 20px !important;
            display: flex;
            align-items: flex-end;
            justify-content: flex-end;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .release {
            width: 16%;
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 7px;
            font-weight: 700;
            text-align: center;
            color: #1C325D;
        }

        .table {
            width: 74% !important;
        }

        .table .box_3 {
            padding: 5px;
            margin-right: 20px;
            border: solid 1px lightgrey;
        }

        .table .box_3 .label {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 5px;
            margin: 0 0 5px 0;
        }

        .table .box_3 .text {
            font-family: 'Source Sans Pro', sans-serif;
            font-size: 8px;
            margin: 0;
            line-height: 1.3;
            text-transform: uppercase;
        }
    </style>

</head>

<body>

    <main class="login" style="padding-top: 50px;" id="certificadoPrint" n-certificado="{{ $certificado->ac_hash_id }}">

        <div class="externa">

            <div class="father">
                <div class="show">
                    <div class="certified">
                        <div class="detail_blue"></div>
                        <div class="imgs" style="width: auto; display: flex; justify-content: flex-end;">
                            <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="" style="width: 13%; height: auto;">
                            <img src="{{ asset('assets/img/cursos/'.$certificado->curso_imagem) }}" style="width: 68%; margin-right:20px;">
                        </div>
                        <div class="infos">
                            <p class="nome">{{ $certificado->aluno_nome }}</p>
                            <p class="subtrace">concluiu com êxito o curso, currículo e as avaliações propostas para o treinamento:</p>
                            <p class="curso">{{ $certificado->curso_titulo }}</p>
                        </div>
                        <div class="cert_rodape">
                            <div class="QRCODE">
                                <img src="{{ asset('assets/img/qr-code/'.$certificado->qr_code) }}">
                            </div>
                            <div class="veracidade">
                                <div class="val">
                                    <div class="box_1">
                                        <p class="label">DATA DE EMISSÃO:</p>
                                        <p class="date">{{ strftime("%d %h %Y", strtotime($certificado->data_emissao)) }}</p>
                                    </div>
                                    <div class="box_2">
                                        <p class="label">VALIDO ATÉ:</p>
                                        <p class="date">{{ strftime("%d %h %Y", strtotime($certificado->data_validade)) }}</p>
                                    </div>
                                    <div class="box_2 codigo">
                                        <p class="label">CÓDIGO DESTE CERTIFICADO:</p>
                                        <p class="date">{{ $certificado->ac_hash_id }}</p>
                                    </div>
                                </div>
                                <p class="consulte">Consulte a veracidade e validade deste documento lendo este QR Code, ou acesse: <a href="https://ecards.cardiol.br">https://ecards.cardiol.br</a> e informe o código do certificado.</p>

                            </div>
                        </div>
                    </div>

                    <div class="certified">
                        <div class="detail_blue"></div>
                        <div class="imgs" style="width: auto; display: flex; justify-content: flex-end;">
                            <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="" style="width: 13%; height: auto;">
                            <img src="{{ asset('assets/img/cursos/'.$certificado->curso_imagem) }}" style="width: 68%; margin-right:20px;">
                        </div>
                        <div class="body">
                            <div class="release">
                                <p>© {{ date('Y') }} Sociedade Brasileira de Cardiologia</p>
                            </div>
                            <div class="table">
                                <div class="box_3">
                                    <P class="label">CURSO</P>
                                    <p class="text">{{ $certificado->curso_titulo }}</p>
                                </div>
                                <div class="box_3">
                                    <P class="label">DIRETOR DO CURSO</P>
                                    <p class="text">{{ $certificado->instrutor_id }} | {{ $certificado->instrutor_nome }}</p>
                                </div>
                                <div class="box_3">
                                    <P class="label">CENTRO DE TREINAMENTO</P>
                                    <p class="text">{{ $certificado->instituicao_nome }}</p>
                                </div>
                                <div class="box_3">
                                    <P class="label">LOCAL</P>
                                    <p class="text">{{ $certificado->cidade }}, {{ $certificado->uf }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-mask-plugin-master/dist/jquery.mask.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>

</body>

</html>