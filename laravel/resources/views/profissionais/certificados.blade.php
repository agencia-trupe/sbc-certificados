@extends('layouts.template')

@section('content')

<main class="login">

    <div class="externa">

        <div class="imagem">
            <div class="img"></div>
            <div class="menus">
                <p>Olá {{ auth('profissionais')->user()->nome }}!  <a href="{{ route('profissionais.edit', auth('profissionais')->user()->id) }}">[ALTERAR SENHA]  </a></p>
                <form action="{{ route('profissionais.logout') }}" method="post" class="wrapper">
                    {!! csrf_field() !!}
                    <button type="submit" class="anchor_sair_menu">SAIR<img src="{{ asset('assets/img/layout/x-sair.svg')}}" class="sair" alt=""></button>
                </form>
            </div>
        </div>

        <div class="father">
            <h1>Sistema E-cards SBC</h1>

            @if(count($certificadosValidos) > 0)
            <div class="card">
                <div class="header">
                    <h2>CERTIFICADOS EMITIDOS VÁLIDOS:</h2>
                </div>
                <div class="body">
                    <div class="wrapper">
                        <div class="hr"></div>

                        @foreach($certificadosValidos as $certificado)
                        <a href="{{ route('profissionais.certificados.show', $certificado->hash_id) }}">
                            <div class="line">
                                <p class="reg">{{ $certificado->ac_hash_id }}</p>
                                <p class="desc">{{ $certificado->curso }}</p>
                                <img src="{{ asset('assets/img/layout/ico-cadastroaluno.svg')}}" alt="">
                            </div>
                        </a>
                        <div class="hr2"></div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif

            @if(count($certificadosRenovacao) > 0)
            <div class="card">
                <div class="header blue">
                    <h2>CERTIFICADOS EMITIDOS EM PERÍODO DE RENOVAÇÃO:</h2>
                </div>
                <div class="body">
                    <div class="wrapper">
                        <div class="hr2"></div>

                        @foreach($certificadosRenovacao as $certificado)
                        <a href="{{ route('profissionais.certificados.show', $certificado->hash_id) }}">
                            <div class="line">
                                <p class="reg">{{ $certificado->ac_hash_id }}</p>
                                <p class="desc">{{ $certificado->curso }}</p>
                                <img src="{{ asset('assets/img/layout/ico-enviar-email-azul.svg')}}" alt="">
                            </div>
                        </a>
                        <div class="hr2"></div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif

            @if(count($certificadosVencidos) > 0)
            <div class="card">
                <div class="header vinho">
                    <h2>CERTIFICADOS VENCIDOS:</h2>
                </div>
                <div class="body">
                    <div class="wrapper">
                        <div class="hr2"></div>

                        @foreach($certificadosVencidos as $certificado)
                        <a href="{{ route('profissionais.certificados.show', $certificado->hash_id) }}">
                            <div class="line">
                                <p class="reg">{{ $certificado->ac_hash_id }}</p>
                                <p class="desc">{{ $certificado->curso }}</p>
                                <img src="{{ asset('assets/img/layout/ico-enviar-email-azul.svg')}}" alt="">
                            </div>
                        </a>
                        <div class="hr2"></div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

    </section>


    @endsection