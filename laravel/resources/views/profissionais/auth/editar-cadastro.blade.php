@extends('layouts.template')

@section('content')

<main class="login">

    <div class="externa">

        <div class="imagem">
            <div class="img"></div>
            <div class="menus">
                <p>Olá {{ auth('profissionais')->user()->nome }}!  <a href="{{ route('profissionais.edit', auth('profissionais')->user()->id) }}">[ALTERAR SENHA]  </a></p>
                <form action="{{ route('profissionais.logout') }}" method="post" class="wrapper">
                    {!! csrf_field() !!}
                    <button type="submit">SAIR<img src="{{ asset('assets/img/layout/x-sair.svg')}}" alt=""></button>
                </form>
            </div>
        </div>

        <div class="father">
            <h1>Sistema E-cards SBC</h1>

            <div class="card">
                @if(auth()->guard('profissionais')->check())
                <form action="{{ route('profissionais.update', $aluno->id) }}" method="post">
                    {!! csrf_field() !!}
                    <div class="campo">
                        <label for="password" class="label">SENHA:</label>
                        <input type="password" id="password" name="password" value="">
                    </div>
                    <div class="campo">
                        <label for="password_confirmation" class="label">REPETIR SENHA:</label>
                        <input type="password" id="password_confirmation" name="password_confirmation" value="">
                    </div>
                    <button type="submit">ALTERAR</button>

                    @include('layouts.flash')

                </form>
                @endif
            </div>

            <a href="{{ route('profissionais.certificados') }}">« voltar</a>
        </div>

    </div>

</main>

@endsection