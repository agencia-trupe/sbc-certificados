@extends('layouts.template')

@section('content')

<main class="login">

    <div class="externa">
        <div class="imagem">
            <div class="img"></div>
        </div>
        <div class="father">
            <h1>Sistema E-cards SBC</h1>

            <div class="logs2">
                <h2>RECUPERAR SENHA</h2>

                <form action="{{ route('profissionais.redefinir-senha.post') }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <input type="hidden" name="email" value="{{ $email }}">
                    <label for="senha2">SENHA</label>
                    <input type="password" id="senha2" name="password">
                    <label for="repetir">REPETIR SENHA</label>
                    <input type="password" id="repetir" name="password_confirmation">
                    <button type="submit">ALTERAR</button>

                    @include('layouts.flash')

                </form>
            </div>
        </div>
    </div>

    </section>


    @endsection