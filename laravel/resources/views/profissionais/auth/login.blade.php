@extends('layouts.template')

@section('content')

<main class="login">

    <div class="externa">
        <div class="imagem">
            <div class="img"></div>
        </div>
        <div class="father">
            <h1>Sistema E-cards SBC</h1>

            <div class="logs2">
                <form action="{{ route('profissionais.login.post') }}" method="post">
                    {!! csrf_field() !!}
                    <label for="login">LOGIN (E-MAIL)</label>
                    <input type="text" id="login" name="email">
                    <label for="password">SENHA</label>
                    <input type="password" id="password" name="password">
                    <button type="submit">ENVIAR</button>

                    @include('layouts.flash')

                    <div class="flex_log">
                        <a href="{{ route('profissionais.esqueci-a-senha') }}">esqueci a senha »</a> | <a href="{{ route('profissionais.cadastro') }}">primeiro cadastro »</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

</main>

@endsection