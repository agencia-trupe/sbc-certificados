@extends('layouts.template')

@section('content')

<main class="login">

    <div class="externa">
        <div class="imagem">
            <div class="img"></div>
        </div>
        <div class="father">
            <h1>Sistema E-cards SBC</h1>

            <div class="logs3">
                <h2>PRIMEIRO CADASTRO • CRIE SUA SENHA</h2>

                <form action="{{ route('profissionais.cadastro.post') }}" method="post">
                    {!! csrf_field() !!}
                    <label for="login2">LOGIN (E-MAIL)*</label>
                    <input type="text" id="login2" name="email">
                    <label for="senha2">SENHA</label>
                    <input type="password" id="senha2" name="password">
                    <label for="repetir">REPETIR SENHA</label>
                    <input type="password" id="repetir" name="password_confirmation">
                    <button type="submit">CADASTRAR</button>
                    <div class="flex_log">
                        <p>(*) O e-mail para cadastro aqui deve ser o mesmo que foi informado quando realizou o curso para que o sistema associe seu login ao certificado correto.</p>
                    </div>

                    @include('layouts.flash')

                </form>
            </div>
        </div>
    </div>

    </section>


    @endsection