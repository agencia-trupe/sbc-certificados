@extends('layouts.template')

@section('content')

<main class="login">

    <div class="externa">

        <div class="imagem">
            <div class="img"></div>
            <div class="menus">
                <p>Olá {{ auth('profissionais')->user()->nome }}!  <a href="{{ route('profissionais.edit', auth('profissionais')->user()->id) }}">[ALTERAR SENHA]  </a></p>
                <form action="{{ route('profissionais.logout') }}" method="post" class="wrapper">
                    {!! csrf_field() !!}
                    <button type="submit">SAIR<img src="{{ asset('assets/img/layout/x-sair.svg')}}" alt=""></button>
                </form>
            </div>
        </div>

        <div class="father print-certificado">
            <h1>Sistema E-cards SBC</h1>

            @if($alunoCertificado->data_validade > date('Y-m-d'))
            <div class="show">
                <h3>SEU CERTIFICADO ESTÁ ATIVO.</h3>
                <div class="certified">
                    <div class="detail_blue"></div>
                    <div class="imgs">
                        <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                        <img src="{{ asset('assets/img/cursos/'.$alunoCertificado->curso_imagem) }}">
                    </div>
                    <div class="infos">
                        <p class="nome">{{ $alunoCertificado->aluno_nome }}</p>
                        <p class="subtrace">concluiu com êxito o curso, currículo e as avaliações propostas para o treinamento:</p>
                        <p class="curso">{{ $alunoCertificado->curso_titulo }}</p>
                    </div>
                    <div class="cert_rodape">
                        <div class="QRCODE">
                            <img src="{{ asset('assets/img/qr-code/'.$alunoCertificado->qr_code) }}">
                        </div>
                        <div class="veracidade">
                            <div class="val">
                                <div class="box_1">
                                    <p class="label">DATA DE EMISSÃO:</p>
                                    <p class="date">{{ strftime("%d %h %Y", strtotime($alunoCertificado->data_emissao)) }}</p>
                                </div>
                                <div class="box_2">
                                    <p class="label">VALIDO ATÉ:</p>
                                    <p class="date">{{ strftime("%d %h %Y", strtotime($alunoCertificado->data_validade)) }}</p>
                                </div>
                                <div class="box_2">
                                    <p class="label">CÓDIGO DESTE CERTIFICADO:</p>
                                    <p class="date">{{ $alunoCertificado->ac_hash_id }}</p>
                                </div>
                            </div>
                            <p class="consulte">Consulte a veracidade e validade deste documento lendo este QR Code, ou acesse: <a href="https://ecards.cardiol.br">https://ecards.cardiol.br</a> e informe o código do certificado.</p>

                        </div>
                    </div>
                </div>

                <div class="certified">
                    <div class="detail_blue"></div>
                    <div class="imgs">
                        <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                        <img src="{{ asset('assets/img/cursos/'.$alunoCertificado->curso_imagem) }}">
                    </div>
                    <div class="body">
                        <div class="release">
                            <p>© {{ date('Y') }} Sociedade Brasileira de Cardiologia</p>
                        </div>
                        <div class="table">
                            <div class="box_3">
                                <P class="label">CURSO</P>
                                <p class="text">{{ $alunoCertificado->curso_titulo }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">DIRETOR DO CURSO</P>
                                <p class="text">{{ $alunoCertificado->instrutor_id }} | {{ $alunoCertificado->instrutor_nome }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">CENTRO DE TREINAMENTO</P>
                                <p class="text">{{ $alunoCertificado->instituicao_nome }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">LOCAL</P>
                                <p class="text">{{ $alunoCertificado->cidade }}, {{ $alunoCertificado->uf }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="buttons">
                    <a href="{{ route('profissionais.certificados.showPrint', $alunoCertificado->ac_hash_id) }}" target="_blank" class="btn-print-certificado">
                        <div class="print">
                            IMPRIMIR
                        </div>
                    </a>

                    <a href="{{ route('profissionais.certificados') }}">
                        <div class="button_voltar">
                            VOLTAR
                        </div>
                    </a>
                </div>
            </div>
            @endif

            @if($alunoCertificado->data_validade <= date('Y-m-d')) <div class="show">
                <h3 class="vinho">SEU CERTIFICADO ESTÁ VENCIDO.</h3>
                <div class="certified">
                    <div class="bg-vencido"></div>
                    <div class="detail_vinho"></div>
                    <div class="imgs">
                        <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                        <img src="{{ asset('assets/img/cursos/'.$alunoCertificado->curso_imagem) }}">
                    </div>
                    <div class="infos">
                        <p class="nome">{{ $alunoCertificado->aluno_nome }}</p>
                        <p class="subtrace">concluiu com êxito o curso, currículo e as avaliações propostas para o treinamento:</p>
                        <p class="curso">{{ $alunoCertificado->curso_titulo }}</p>
                    </div>
                    <div class="cert_rodape">
                        <div class="QRCODE">
                            <img src="{{ asset('assets/img/qr-code/'.$alunoCertificado->qr_code) }}">
                        </div>
                        <div class="veracidade">
                            <div class="val">
                                <div class="box_1">
                                    <p class="label">DATA DE EMISSÃO:</p>
                                    <p class="date">{{ strftime("%d %h %Y", strtotime($alunoCertificado->data_emissao)) }}</p>
                                </div>
                                <div class="box_2">
                                    <p class="label">VALIDO ATÉ:</p>
                                    <p class="date">{{ strftime("%d %h %Y", strtotime($alunoCertificado->data_validade)) }}</p>
                                </div>
                                <div class="box_2">
                                    <p class="label">CÓDIGO DESTE CERTIFICADO:</p>
                                    <p class="date">{{ $alunoCertificado->ac_hash_id }}</p>
                                </div>
                            </div>
                            <p class="consulte">Consulte a veracidade e validade deste documento lendo este QR Code, ou acesse: <a href="https://ecards.cardiol.br">https://ecards.cardiol.br</a> e informe o código do certificado.</p>

                        </div>
                    </div>
                </div>

                <div class="certified">
                    <div class="bg-vencido"></div>
                    <div class="detail_vinho"></div>
                    <div class="imgs">
                        <img src="{{ asset('assets/img/layout/marca-SBC-selo.svg')}}" alt="">
                        <img src="{{ asset('assets/img/cursos/'.$alunoCertificado->curso_imagem) }}">
                    </div>
                    <div class="body">
                        <div class="release">
                            <p>© {{ date('Y') }} Sociedade Brasileira de Cardiologia</p>
                        </div>
                        <div class="table">
                            <div class="box_3">
                                <P class="label">CURSO</P>
                                <p class="text">{{ $alunoCertificado->curso_titulo }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">DIRETOR DO CURSO</P>
                                <p class="text">{{ $alunoCertificado->instrutor_id }} | {{ $alunoCertificado->instrutor_nome }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">CENTRO DE TREINAMENTO</P>
                                <p class="text">{{ $alunoCertificado->instituicao_nome }}</p>
                            </div>
                            <div class="box_3">
                                <P class="label">LOCAL</P>
                                <p class="text">{{ $alunoCertificado->cidade }}, {{ $alunoCertificado->uf }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="buttons">
                    <a href="{{ route('profissionais.certificados.showPrint', $alunoCertificado->ac_hash_id) }}" target="_blank" class="btn-print-certificado">
                        <div class="print">
                            IMPRIMIR
                        </div>
                    </a>

                    <a href="{{ route('profissionais.certificados') }}">
                        <div class="button_voltar">
                            VOLTAR
                        </div>
                    </a>
                </div>
        </div>
        @endif
    </div>
    </div>

    </section>


    @endsection