import MobileToggle from "./MobileToggle";
import moment from "moment";

MobileToggle();

$(window).on("load", function () {});

$(document).ready(function () {
    var urlPrevia = "";
    // var urlPrevia = "/previa-sbccertificados";

    $(".btn-upload-imagem-curso").click(function () {
        var document_id = $(this).attr("id");
        $("input[name=" + document_id + "]").click();
    });

    // CERTIFICADOS - data validade = 2 anos + data de emissão preenchida
    $('input[name="data_emissao"]').on("blur", function () {
        var dataEmissao = $(this).val();
        var dataValidade = moment(dataEmissao).add(2, "y").format("YYYY-MM-DD");
        $('input[name="data_validade"]').val(dataValidade);
    });

    // CERTIFICADOS - instituição parceira
    $('input[name="instituicao_parceira"]').on("click", function () {
        if ($(this).is(":checked")) {
            $(".dados-instituicao-parceira").css("display", "block");
        } else {
            $(".dados-instituicao-parceira").css("display", "none");
        }
    });

    // CERTIFICADOS - ajax para buscar aluno por cpf
    $(".busca .link-buscar-cpf").on("click", function (event) {
        event.preventDefault();
        var cpf = $('.busca input[name="cpf"]').val();
        var url =
            window.location.origin +
            urlPrevia +
            "/admin/certificados/aluno/" +
            cpf;
        console.log(url);

        $.ajax({
            type: "GET",
            url: url,
            beforeSend: function () {},
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                if (data.aluno != null) {
                    $(".input-aluno")
                        .attr(
                            "placeholder",
                            data.aluno.nome + " | " + data.aluno.email
                        )
                        .attr("hashId", data.aluno.hash_id);
                } else {
                    $(".input-aluno")
                        .attr("placeholder", data.erro)
                        .attr("hashId", null);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });
    $(".input-aluno").val($(".input-aluno").attr("hashId"));

    // CERTIFICADOS - ajax para inserir aluno neste certificado (se atualizar a página sem salvar o certificado geral, perde a inserção de alunos)
    $("body").on("click", ".link-add-aluno-certificado", function (event) {
        event.preventDefault();
        var token = $(".form-add-aluno-certificado meta[name=_token]").attr(
            "content"
        );
        var alunoHashId = $(".form-add-aluno-certificado .input-aluno").attr(
            "hashId"
        );
        var certificadoHashId = $(
            ".form-add-aluno-certificado .certificado"
        ).val();
        var notaProvaTeorica = $(
            ".form-add-aluno-certificado input.nota-teorica"
        ).val();
        var notaProvaPratica = $(
            ".form-add-aluno-certificado input.nota-pratica"
        ).val();
        var notaFinal = $(".form-add-aluno-certificado input.nota-final").val();

        var url =
            window.location.origin +
            urlPrevia +
            "/admin/certificados/aluno/adicionar";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                aluno_id: alunoHashId,
                certificado_id: certificadoHashId,
                nota_prova_teorica: notaProvaTeorica,
                nota_prova_pratica: notaProvaPratica,
                nota_final: notaFinal,
            },
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                $('.busca input[name="cpf"]').val("");
                $(".form-add-aluno-certificado input.nota-teorica").val("");
                $(".form-add-aluno-certificado input.nota-pratica").val("");
                $(".form-add-aluno-certificado input.nota-final").val("");
                $(".input-aluno").attr("placeholder", "");

                if (response.success == true) {
                    $(".flash-ajax")
                        .html("<p>" + response.message + "</p>")
                        .addClass("flash-sucesso")
                        .css("display", "flex");
                    setTimeout(function () {
                        document.location.reload(true);
                    }, 1000);
                } else {
                    $(".flash-ajax")
                        .html("<p>" + response.message + "</p>")
                        .addClass("flash-erro")
                        .css("display", "flex");
                }

                var htmlAddAluno =
                    '<div class="newLine" id="' +
                    response.alunoCertificado.id +
                    '"><p>' +
                    response.aluno.nome +
                    "</p><p>" +
                    response.aluno.email +
                    '</p><div class="flex2"><p>' +
                    response.alunoCertificado.nota_prova_teorica +
                    "</p>|<p>" +
                    response.alunoCertificado.nota_prova_pratica +
                    "</p>|<p>" +
                    response.alunoCertificado.nota_final +
                    '</p></div><a href="" class="email status-emails"></a></div>';
                $(".resultados-alunos-certificados").append(htmlAddAluno);

                if (
                    response.alunoCertificado.certificado_emitido == 1 &&
                    response.alunoCertificado.data_envio_certificado != null
                ) {
                    var statusEmailsEnviados =
                        "<p class='status-emails'>[e-mail enviado " +
                        response.aluno.data_envio_certificado +
                        "]</p>";
                    $(
                        ".resultados-alunos-certificados .newLine#" +
                            response.alunoCertificado.id
                    ).append(statusEmailsEnviados);
                } else {
                    var statusEmails =
                        '<p class="email status-emails">[ ENVIO DE E-MAIL PENDENTE ]</p><a href="" class="link-excluir-aluno-certificado" hashId="' +
                        response.aluno.hash_id +
                        '"><img src="' +
                        window.location.origin +
                        urlPrevia +
                        "/assets/img/layout/x-sair.svg" +
                        '" class="sair" alt=""></a>';
                    $(
                        ".resultados-alunos-certificados .newLine#" +
                            response.alunoCertificado.id
                    ).append(statusEmails);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
                $(".flash-ajax")
                    .html("<p>Erro ao adicionar o aluno neste certificado.</p>")
                    .addClass("flash-erro")
                    .css("display", "flex");
            },
        });
    });

    // CERTIFICADOS - ajax para excluir aluno deste certificado (botão só é exibido se certificado_emitido = 0 )
    $("body").on("click", ".link-excluir-aluno-certificado", function (event) {
        event.preventDefault();

        if (
            confirm("Tem certeza que deseja excluir o aluno do certificado?") ==
            true
        ) {
            var certificadoHashId = $(".form-certificado").attr("id");
            var alunoHashId = $(this).attr("hashId");
            var url =
                window.location.origin +
                urlPrevia +
                "/admin/certificados/" +
                certificadoHashId +
                "/aluno/" +
                alunoHashId +
                "/excluir";
            console.log(url);

            $.ajax({
                type: "DELETE",
                url: url,
                beforeSend: function () {},
                success: function (response, textStatus, jqXHR) {
                    console.log(response);
                    $(".newLine#" + response.id).remove();
                    if (response.success == true) {
                        $(".flash-ajax")
                            .html("<p>" + response.message + "</p>")
                            .addClass("flash-sucesso")
                            .css("display", "flex");
                    } else {
                        $(".flash-ajax")
                            .html("<p>" + response.message + "</p>")
                            .addClass("flash-erro")
                            .css("display", "flex");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                    $(".flash-ajax")
                        .html(
                            "<p>Erro ao remover o aluno deste certificado.</p>"
                        )
                        .addClass("flash-erro")
                        .css("display", "flex");
                },
            });
        }
    });

    // CERTIFICADOS - ajax para enviar emails e setar certificado_emitido
    $("body").on("click", ".link-enviar-certificados-email", function (event) {
        event.preventDefault();

        if (confirm("Tem certeza que deseja enviar os certificados?") == true) {
            var certificadoHashId = $(this).attr("hashId");
            var url =
                window.location.origin +
                urlPrevia +
                "/admin/certificados/" +
                certificadoHashId +
                "/enviar-emails";

            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function () {},
                success: function (response, textStatus, jqXHR) {
                    if (response.success == true) {
                        $(".flash-ajax")
                            .html("<p>" + response.message + "</p>")
                            .addClass("flash-sucesso")
                            .css("display", "flex");
                        setTimeout(function () {
                            document.location.reload(true);
                        }, 2000);
                    } else {
                        $(".flash-ajax")
                            .html("<p>" + response.message + "</p>")
                            .addClass("flash-erro")
                            .css("display", "flex");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                    $(".flash-ajax")
                        .html(
                            "<p>Erro ao enviar e-mails e atualizar certificado emitido</p>"
                        )
                        .removeClass("flash-sucesso")
                        .addClass("flash-erro")
                        .css("display", "flex");
                },
            });
        }
    });

    // VISÃO GERAL - ajax para enviar emails de vencimento
    $("body").on("click", ".link-enviar-emails-vencimento", function (event) {
        event.preventDefault();

        if (
            confirm(
                "Tem certeza que deseja enviar os e-mails sobre o vencimento do certificado?"
            ) == true
        ) {
            var certificadoHashId = $(this).attr("hashId");
            var dataAviso = $(".input-data-aviso-de-vencimento input").val();
            var url =
                window.location.origin +
                urlPrevia +
                "/admin/visao-geral/" +
                certificadoHashId +
                "/aviso-de-vencimento";

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    data_aviso_vencimento_enviado: dataAviso,
                },
                beforeSend: function () {},
                success: function (response, textStatus, jqXHR) {
                    console.log(response);
                    if (response.success == true) {
                        $(".flash-ajax")
                            .html("<p>" + response.message + "</p>")
                            .addClass("flash-sucesso")
                            .css("display", "flex");
                        setTimeout(function () {
                            document.location.reload(true);
                        }, 2000);
                    } else {
                        $(".flash-ajax")
                            .html("<p>" + response.message + "</p>")
                            .addClass("flash-erro")
                            .css("display", "flex");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                },
            });
        }
    });

    // VISÃO GERAL - ajax para adicionar a data do envio de aviso de vencimento manual
    $("body").on("click", ".link-adicionar-data-do-envio", function (event) {
        event.preventDefault();

        $(this).css("display", "none");
        $(".link-enviar-emails-vencimento").css("display", "none");
        $(".input-data-aviso-de-vencimento.adicionar").css("display", "flex");
    });

    // VISÃO GERAL - ajax para alterar a data do envio de aviso de vencimento
    $("body").on("click", ".link-editar-data-do-envio", function (event) {
        event.preventDefault();

        $(this).css("display", "none");
        $(".input-data-aviso-de-vencimento").css("display", "flex");
    });

    $("body").on(
        "click",
        ".link-input-alterar-data-do-envio",
        function (event) {
            event.preventDefault();

            if (
                confirm(
                    "Tem certeza que deseja alterar a data de envio dos avisos de vencimentos deste certificado?"
                ) == true
            ) {
                var certificadoHashId = $(this).attr("hashId");
                var dataAviso = $(this).parent().children("input").val();
                var token = $(
                    ".input-data-aviso-de-vencimento meta[name=_token]"
                ).attr("content");
                var url =
                    window.location.origin +
                    urlPrevia +
                    "/admin/visao-geral/" +
                    certificadoHashId +
                    "/aviso-de-vencimento/editar";
                console.log(dataAviso);

                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        _token: token,
                        data_aviso_vencimento_enviado: dataAviso,
                    },
                    beforeSend: function () {},
                    success: function (response, textStatus, jqXHR) {
                        if (response.success == true) {
                            $(".flash-ajax")
                                .html("<p>" + response.message + "</p>")
                                .addClass("flash-sucesso")
                                .css("display", "flex");
                        } else {
                            $(".flash-ajax")
                                .html("<p>" + response.message + "</p>")
                                .addClass("flash-erro")
                                .css("display", "flex");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".flash-ajax")
                            .html("<p>ERRO</p>")
                            .addClass("flash-erro")
                            .css("display", "flex");
                        console.log(jqXHR, textStatus, errorThrown);
                    },
                });
            }
        }
    );

    // ALUNOS - ajax para reenviar email do certificado e setar certificado_emitido
    $("body").on("click", ".link-reenviar-certificado-aluno", function (event) {
        event.preventDefault();

        if (confirm("Tem certeza que deseja renviar o certificado?") == true) {
            var certificadoHashId = $(this).attr("certificadoId");
            var alunoHashId = $(this).attr("alunoId");
            var token = $("meta[name=_token]").attr("content");
            var url =
                window.location.origin +
                urlPrevia +
                "/admin/alunos/" +
                alunoHashId +
                "/certificados/" +
                certificadoHashId +
                "/reenviar";

            $.ajax({
                type: "GET",
                url: url,
                data: {
                    _token: token,
                },
                beforeSend: function () {},
                success: function (response, textStatus, jqXHR) {
                    if (response.success == true) {
                        $(".flash-ajax")
                            .html("<p>" + response.message + "</p>")
                            .addClass("flash-sucesso")
                            .css("display", "flex");
                    } else {
                        $(".flash-ajax")
                            .html("<p>" + response.message + "</p>")
                            .addClass("flash-erro")
                            .css("display", "flex");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                    $(".flash-ajax")
                        .html("<p>Erro ao reenviar certificado.</p>")
                        .addClass("flash-erro")
                        .css("display", "flex");
                },
            });
        }
    });

    // ALUNOS - import excel
    $(".btn-upload-excel").click(function () {
        var document_id = $(this).attr("id");
        $("input[name=" + document_id + "]").click();
    });

    $("body").on("change", "input[name=excel_alunos]", function () {
        $(".doc-selecionado")
            .html("<strong>Arquivo selecionado: </strong>" + $(this).val());
        $(".info-importar").css("display", "flex");
    });

    // RELATÓRIOS - input radio select curso/instituição
    $("#filtro_cursos").on("change", function (event) {
        event.preventDefault();
        if ($(this).is(":checked")) {
            $("#filtro_instituicoes").prop("checked", false);
            $(".select-instituicoes").css("display", "none");
            $(".select-cursos").css("display", "block");
        }
    });
    $("#filtro_instituicoes").on("change", function (event) {
        event.preventDefault();
        if ($(this).is(":checked")) {
            $("#filtro_cursos").prop("checked", false);
            $(".select-cursos").css("display", "none");
            $(".select-instituicoes").css("display", "block");
        }
    });

    // RELATÓRIOS - ajax para relatório de totais
    $("body").on("click", ".btn-consultar-totais", function (event) {
        event.preventDefault();

        var dataInicio =
            $("input#data_inicio").val() != ""
                ? $("input#data_inicio").val()
                : alert("Preencha a data de início.");
        var dataFim =
            $("input#data_fim").val() != ""
                ? $("input#data_fim").val()
                : alert("Preencha a data de fim.");
        var token = $("meta[name=_token]").attr("content");
        console.log(dataInicio, dataFim, token);

        var url =
            window.location.origin + urlPrevia + "/admin/relatorios/totais";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                _token: token,
                data_inicio: dataInicio,
                data_fim: dataFim,
            },
            beforeSend: function () {
                $(".relatorios .resultados.relatorio").css("display", "none");
            },
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                if (response.success == true) {
                    $(".periodo .inicio").html(response.dataInicio);
                    $(".periodo .fim").html(response.dataFim);
                    $(".n-certificados span").html(
                        response.totalCertificados > 1
                            ? response.totalCertificados + " cursos"
                            : response.totalCertificados + " curso"
                    );
                    $(".n-alunos-cursos span").html(
                        response.totalAlunos > 1
                            ? response.totalAlunos + " alunos"
                            : response.totalAlunos + " aluno"
                    );
                    $(".n-instituicoes span").html(
                        response.totalInstituicoes > 1
                            ? response.totalInstituicoes + " instituições"
                            : response.totalInstituicoes + " instituição"
                    );
                    $(".n-vencimentos span").html(
                        response.totalVencimentos > 1
                            ? response.totalVencimentos + " cursos"
                            : response.totalVencimentos + " curso"
                    );
                    $(".media-alunos span").html(
                        response.mediaAlunos + " alunos"
                    );

                    $(".relatorios .resultados.totais").css("display", "block");
                } else {
                    $(".flash-ajax")
                        .html("<p>" + response.message + "</p>")
                        .addClass("flash-erro")
                        .css("display", "flex");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
                $(".flash-ajax")
                    .html("<p>Erro ao adicionar o aluno neste certificado.</p>")
                    .addClass("flash-erro")
                    .css("display", "flex");
            },
        });
    });

    // MASK telefone
    $(".input-telefone").mask("(00) 000000000");
    $(".input-cpf").mask("000.000.000-00");

    // AVISO DE COOKIES
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
        var url = window.location.origin + urlPrevia + "/aceite-de-cookies";

        $.ajax({
            type: "POST",
            url: url,
            success: function (data, textStatus, jqXHR) {
                $(".aviso-cookies").hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });

    $(window).on("load", function () {
        var html = $("#certificadoPrint").get(0);
        var fileName =
            "Certificado-" +
            $("#certificadoPrint").attr("n-certificado") +
            ".pdf";
        var pdf = new jsPDF("p", "mm", [297, 200]);
        pdf.addHTML(
            html,
            20,
            0,
            {
                pagesplit: true,
            },
            function (dispose) {
                pdf.save(fileName);
            }
        );
    });
});
