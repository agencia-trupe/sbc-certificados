<?php

namespace App\Exports;

use App\Models\AlunoCertificado;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class MailingExport implements FromCollection, WithHeadings
{
    protected $hash_id;

    function __construct($hash_id)
    {
        $this->hash_id = $hash_id;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $alunosCertificados = AlunoCertificado::join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
            ->select('alunos_certificados.hash_id as ac_hash_id', 'alunos.nome', 'alunos.cpf', 'alunos.email')
            ->where('certificados.hash_id', $this->hash_id)->get();

        return $alunosCertificados;
    }

    public function headings(): array
    {
        return ['ID Aluno/Certificado', 'Aluno', 'CPF', 'E-mail'];
    }
}
