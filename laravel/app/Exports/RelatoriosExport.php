<?php

namespace App\Exports;

use App\Models\AlunoCertificado;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RelatoriosExport implements FromCollection, WithHeadings
{
    protected $hash_id;

    function __construct($hash_id)
    {
        $this->hash_id = $hash_id;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $alunosCertificados = AlunoCertificado::join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->join('instrutores', 'instrutores.id', '=', 'certificados.instrutor_id')
            ->join('instituicoes', 'instituicoes.id', '=', 'certificados.instituicao_id')
            ->select('alunos_certificados.hash_id as ac_hash_id', 'alunos.nome', 'alunos.cpf', 'alunos.email', 'cursos.titulo as curso', 'certificados.data_emissao', 'certificados.data_validade', 'instrutores.nome as instrutor_nome', 'instituicoes.nome as instituicao_nome', 'alunos_certificados.certificado_emitido', 'alunos_certificados.data_envio_certificado', 'alunos_certificados.nota_prova_teorica', 'alunos_certificados.nota_prova_pratica', 'alunos_certificados.nota_final')
            ->where('certificados.hash_id', $this->hash_id)->get();

        return $alunosCertificados;
    }

    public function headings(): array
    {
        return ['ID Aluno/Certificado', 'Aluno', 'CPF', 'E-mail', 'Curso', 'Data Emissão', 'Data Validade', 'Instrutor', 'Instituição', 'Certificado Enviado', 'Data de Envio', 'Nota Prova Teórica', 'Nota Prova Prática', 'Nota Final'];
    }
}
