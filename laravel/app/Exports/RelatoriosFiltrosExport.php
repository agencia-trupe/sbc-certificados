<?php

namespace App\Exports;

use App\Models\Certificado;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RelatoriosFiltrosExport implements FromCollection, WithHeadings
{
    protected $dataInicio;
    protected $dataFim;
    protected $filtro;
    protected $cursoHashId;
    protected $instituicaoHashId;

    function __construct($dataInicio, $dataFim, $filtro, $cursoHashId, $instituicaoHashId)
    {
        $this->dataInicio = $dataInicio;
        $this->dataFim = $dataFim;
        $this->filtro = $filtro;
        $this->cursoHashId = $cursoHashId;
        $this->instituicaoHashId = $instituicaoHashId;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $certificados = Certificado::whereBetween('data_emissao', [$this->dataInicio, $this->dataFim])
            ->join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
            ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->join('instituicoes', 'instituicoes.id', '=', 'certificados.instituicao_id')
            ->join('instrutores', 'instrutores.id', '=', 'certificados.instrutor_id')
            ->where('alunos_certificados.certificado_emitido', 1)
            ->select('certificados.hash_id', 'cursos.titulo as curso_nome', 'certificados.data_emissao', 'instituicoes.nome as instituicao_nome', 'certificados.cidade', 'certificados.uf', 'instrutores.nome as instrutor_nome', 'alunos_certificados.hash_id as ac_hash_id', 'alunos.nome as aluno_nome', 'alunos.cpf as aluno_cpf', 'alunos.email as aluno_email', 'alunos.profissao as aluno_profissao', 'alunos_certificados.nota_prova_teorica', 'alunos_certificados.nota_prova_pratica', 'alunos_certificados.nota_final')
            ->groupBy('certificados.hash_id', 'alunos_certificados.hash_id', 'alunos.nome', 'alunos.cpf', 'alunos.email', 'alunos_certificados.nota_prova_teorica', 'alunos_certificados.nota_prova_pratica', 'alunos_certificados.nota_final')
            ->orderBy('cursos.titulo');

        if ($this->filtro == 'cursos' && !empty($this->cursoHashId)) {
            $certificados = $certificados->where('cursos.hash_id', $this->cursoHashId)->get();
        } elseif ($this->filtro == 'instituicoes' && !empty($this->instituicaoHashId)) {
            $certificados = $certificados->where('instituicoes.hash_id', $this->instituicaoHashId)->get();
        } else {
            $certificados = $certificados->get();
        }

        return $certificados;
    }

    public function headings(): array
    {
        return ['ID Certificado/Turma', 'Nome do Curso', 'Data', 'Instituição', 'Cidade', 'UF', 'Instrutor', 'ID Certificado-Aluno', 'Nome do Aluno', 'CPF', 'E-mail', 'Profissão', 'Nota Prova Teórica', 'Nota Prova Prática', 'Nota Final'];
    }
}
