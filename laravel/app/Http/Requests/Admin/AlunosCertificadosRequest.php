<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AlunosCertificadosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'aluno_id' => 'required',
            'certificado_id' => 'required',
            'nota_prova_teorica' => 'required|numeric|min:0|max:100',
            'nota_prova_pratica' => 'required|numeric|min:0|max:100',
            'nota_final'         => 'required|numeric|min:0|max:100',
        ];
    }
}
