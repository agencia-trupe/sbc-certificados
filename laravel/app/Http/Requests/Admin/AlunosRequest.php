<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AlunosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cpf'       => 'sometimes|required',
            'nome'      => 'required|max:255',
            'email'     => 'sometimes|email|max:255',
            'password'  => 'sometimes|confirmed|min:8|nullable',
            'profissao' => 'sometimes|nullable',
        ];
    }
}
