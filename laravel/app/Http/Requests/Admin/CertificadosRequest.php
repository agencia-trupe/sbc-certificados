<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CertificadosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data_emissao'   => 'required',
            'data_validade'  => 'required',
            'curso_id'       => 'required',
            'instituicao_id' => 'required',
            'cidade'         => 'required',
            'uf'             => 'required',
            'instrutor_id'   => 'required',
        ];
    }
}
