<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AreaAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tempo_aviso_vencimento'  => 'required',
            'texto_email_vencimento'  => 'required',
            'texto_email_certificado' => 'required',
        ];
    }
}
