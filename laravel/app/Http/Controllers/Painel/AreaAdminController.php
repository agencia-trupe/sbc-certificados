<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AreaAdminRequest;
use App\Models\AreaAdmin;
use Illuminate\Http\Request;

class AreaAdminController extends Controller
{
    public function index()
    {
        $area_admin = AreaAdmin::first();

        return view('painel.area-admin.edit', compact('area_admin'));
    }

    public function update(AreaAdminRequest $request, AreaAdmin $area_admin)
    {
        try {
            $input = $request->all();

            $area_admin->update($input);

            return redirect()->route('painel.area-admin.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
