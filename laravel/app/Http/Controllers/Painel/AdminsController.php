<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminsRequest;
use App\Models\Admin;
use Illuminate\Http\Request;

class AdminsController extends Controller
{
    public function index()
    {
        $admins = Admin::all();

        return view('painel.admins.index', compact('admins'));
    }

    public function create()
    {
        return view('painel.admins.create');
    }

    public function store(AdminsRequest $request)
    {
        try {
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);

            Admin::create($input);

            return redirect()->route('painel.admins.index')->with('success', 'Usuário admin adicionado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar usuário: ' . $e->getMessage()]);
        }
    }

    public function edit(Admin $admin)
    {
        return view('painel.admins.edit', compact('admin'));
    }

    public function update(AdminsRequest $request, Admin $admin)
    {
        try {
            $input = $request->all();
            if (isset($input['password'])) $input['password'] = bcrypt($input['password']);

            $admin->update($input);

            return redirect()->route('painel.admins.index')->with('success', 'Usuário admin alterado com sucesso.');
        } catch (\Throwable $e) {
            return back()->withErrors(['Erro ao alterar usuário: ' . $e->getMessage()]);
        }
    }

    public function destroy(Admin $admin)
    {
        try {
            $admin->delete();

            return redirect()->route('painel.admins.index')->with('success', 'Usuário admin excluído com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir usuário: ' . $e->getMessage()]);
        }
    }
}
