<?php

namespace App\Http\Controllers\Painel;

use App\Helpers\CropImage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PainelController extends Controller
{
    public function index()
    {
        return view('painel.home-painel');
    }

    public function order(Request $request)
    {
        if (!$request->ajax()) return false;

        $data  = $request->input('data');
        $table = $request->input('table');

        for ($i = 0; $i < count($data); $i++) {
            DB::table($table)->where('id', $data[$i])->update(array('ordem' => $i + 1));
        }

        return json_encode($data);
    }

    public function imageUpload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'upload' => 'image|required'
        ]);

        if ($validator->fails()) {
            $response = [
                'error'   => [
                    'message' => 'O arquivo deve ser uma imagem.'
                ]
            ];
        } else {
            $imagem   = CropImage::make('upload', [
                'width'  => null,
                'height' => null,
                'path'   => 'assets/img/ckeditor/'
            ]);
            $response = [
                'url' => asset('assets/img/ckeditor/' . $imagem)
            ];
        }

        return response()->json($response);
    }
}
