<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\AlunoCertificado;
use Illuminate\Http\Request;

class ConsultaController extends Controller
{
    public function index()
    {
        return view('consulta');
    }

    public function show($hash_id)
    {
        $certificado = AlunoCertificado::join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
            ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->join('instrutores', 'instrutores.id', '=', 'certificados.instrutor_id')
            ->join('instituicoes', 'instituicoes.id', '=', 'certificados.instituicao_id')
            ->select('alunos.nome as aluno_nome', 'certificados.hash_id', 'certificados.data_emissao', 'certificados.data_validade', 'certificados.cidade', 'certificados.uf', 'alunos_certificados.hash_id as ac_hash_id', 'cursos.titulo as curso_titulo', 'cursos.imagem as curso_imagem', 'instrutores.nome as instrutor_nome', 'instrutores.hash_id as instrutor_id', 'instituicoes.nome as instituicao_nome', 'alunos_certificados.qr_code')
            ->where('alunos_certificados.hash_id', $hash_id)
            ->where('alunos_certificados.certificado_emitido', 1)
            ->first();

        return view('resultado', compact('certificado'));
    }

    public function post(Request $request)
    {
        try {
            $hash_id = $request->get('codigo_certificado');

            $certificado = AlunoCertificado::join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
                ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
                ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
                ->join('instrutores', 'instrutores.id', '=', 'certificados.instrutor_id')
                ->join('instituicoes', 'instituicoes.id', '=', 'certificados.instituicao_id')
                ->select('alunos.nome as aluno_nome', 'certificados.hash_id', 'certificados.data_emissao', 'certificados.data_validade', 'certificados.cidade', 'certificados.uf', 'alunos_certificados.hash_id as ac_hash_id', 'cursos.titulo as curso_titulo', 'cursos.imagem as curso_imagem', 'instrutores.nome as instrutor_nome', 'instrutores.hash_id as instrutor_id', 'instituicoes.nome as instituicao_nome', 'alunos_certificados.qr_code')
                ->where('alunos_certificados.hash_id', $hash_id)
                ->where('alunos_certificados.certificado_emitido', 1)
                ->first();

            return view('resultado', compact('certificado'));
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao consultar certificado.']);
        }
    }

    public function showPrint($hash_id)
    {
        $certificado = AlunoCertificado::join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
            ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->join('instrutores', 'instrutores.id', '=', 'certificados.instrutor_id')
            ->join('instituicoes', 'instituicoes.id', '=', 'certificados.instituicao_id')
            ->select('alunos.nome as aluno_nome', 'certificados.hash_id', 'certificados.data_emissao', 'certificados.data_validade', 'certificados.cidade', 'certificados.uf', 'alunos_certificados.hash_id as ac_hash_id', 'cursos.titulo as curso_titulo', 'cursos.imagem as curso_imagem', 'instrutores.nome as instrutor_nome', 'instrutores.hash_id as instrutor_id', 'instituicoes.nome as instituicao_nome', 'alunos_certificados.qr_code')
            ->where('alunos_certificados.hash_id', $hash_id)
            ->where('alunos_certificados.certificado_emitido', 1)
            ->first();

        if ($certificado->data_validade > date('Y-m-d')) {
            return view('resultado-valido', compact('certificado'));
        }
        if ($certificado->data_validade <= date('Y-m-d')) {
            return view('resultado-vencido', compact('certificado'));
        }
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
