<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CursosRequest;
use App\Models\Certificado;
use App\Models\Curso;
use Illuminate\Http\Request;

class CursosController extends Controller
{
    private function createHashId()
    {
        $hashId = str_pad(random_int(100000, 999999), 6, 0, STR_PAD_LEFT);
        $check = Curso::where('hash_id', $hashId)->get();
        if (count($check) > 0) {
            return $this->checkHashId();
        } else {
            return $hashId;
        }
    }

    public function index()
    {
        $cursos = Curso::orderBy('titulo', 'asc')->paginate(20);
        $certificados = Certificado::get();

        return view('admin.cursos.index', compact('cursos', 'certificados'));
    }

    public function create()
    {
        return view('admin.cursos.form');
    }

    public function store(CursosRequest $request)
    {
        try {
            $input = $request->all();
            $hash = $this->createHashId();
            $input['hash_id'] = $hash;

            if (isset($input['imagem'])) $input['imagem'] = Curso::upload_imagem();

            Curso::create($input);

            return redirect()->route('admin.cursos.index')->with('success', 'Curso adicionado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar curso: ' . $e->getMessage()]);
        }
    }

    public function edit(Curso $curso)
    {
        return view('admin.cursos.form', compact('curso'));
    }

    public function update(CursosRequest $request, Curso $curso)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Curso::upload_imagem();

            $curso->update($input);

            return redirect()->route('admin.cursos.index')->with('success', 'Curso atualizado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao editar curso: ' . $e->getMessage()]);
        }
    }

    public function delete(Curso $curso)
    {
        try {
            $curso->delete();

            return redirect()->route('admin.cursos.index')->with('success', 'Curso excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir curso: ' . $e->getMessage()]);
        }
    }
}
