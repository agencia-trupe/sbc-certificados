<?php

namespace App\Http\Controllers\Admin;

use App\Exports\MailingExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DatasAvisosDeVencimentosRequest;
use App\Models\AlunoCertificado;
use App\Models\AreaAdmin;
use App\Models\Certificado;
use App\Notifications\VencimentoCertificadosNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class VisaoGeralController extends Controller
{
    public function index()
    {
        $tempoVencimento = AreaAdmin::first()->tempo_aviso_vencimento;
        $dataAtual = date('Y-m-d');
        $dataAvisoVencimento = date('Y-m-d', strtotime("+6 months", strtotime($dataAtual)));

        $ultimosCertificados = Certificado::where('data_validade', '>', $dataAtual)
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
            ->select('certificados.hash_id', 'certificados.data_emissao', 'cursos.titulo as curso')
            ->where('alunos_certificados.certificado_emitido', 1)
            ->orderBy('data_emissao', 'desc')
            ->distinct()->take(8)->get();

        $certificadosVencimentoProximo = Certificado::where('data_validade', '<=', $dataAvisoVencimento)
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
            ->select('certificados.hash_id', 'certificados.data_validade', 'certificados.aviso_vencimento_enviado', 'cursos.titulo as curso')
            ->where('alunos_certificados.certificado_emitido', 1)
            ->orderBy('data_validade', 'desc')
            ->distinct()->get();

        return view('admin.visao-geral.index', compact('ultimosCertificados', 'certificadosVencimentoProximo'));
    }

    public function show(Certificado $certificado)
    {
        $certificadoInfos = Certificado::join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->join('instrutores', 'instrutores.id', '=', 'certificados.instrutor_id')
            ->select('certificados.hash_id', 'certificados.data_emissao', 'certificados.data_validade', 'certificados.cidade', 'certificados.uf', 'certificados.aviso_vencimento_enviado', 'certificados.data_aviso_vencimento_enviado', 'cursos.titulo as curso', 'instrutores.nome as instrutor_nome', 'instrutores.hash_id as instrutor_id')
            ->where('certificados.hash_id', $certificado->hash_id)
            ->first();

        return view('admin.visao-geral.show', compact('certificadoInfos'));
    }

    public function sendEmailsVencimento(Request $request, $hash_id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'data_aviso_vencimento_enviado' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => true, 'message' => 'Preencha todos os campos corretamente.']);
            } else {
                $certificado = Certificado::where('hash_id', $hash_id)->first();

                $alunosEmails = AlunoCertificado::where('certificado_id', $certificado->id)
                    ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
                    ->join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
                    ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
                    ->select('alunos.*', 'cursos.titulo as curso', 'alunos_certificados.hash_id as ac_hash_id', 'alunos_certificados.certificado_emitido as ac_hash_id', 'alunos_certificados.certificado_emitido', 'certificados.instituicao_parceira', 'certificados.nome_instituicao', 'certificados.email_instituicao', 'certificados.texto_instituicao')->get();

                $textoEmail = AreaAdmin::first()->texto_email_vencimento;

                foreach ($alunosEmails as $aluno) {
                    $data = [
                        'textoEmail' => $textoEmail,
                        'aluno'  => $aluno,
                    ];

                    Mail::send('emails.certificados-vencimento', $data, function ($m) use ($data) {
                        $m->to($data['aluno']->email, $data['aluno']->nome)
                            ->bcc('laura@trupe.net')
                            ->subject('[AVISO DE VENCIMENTO | CERTIFICADO | Curso: ' . $data['aluno']->curso . '] ' . config('app.name'));
                    });
                }

                $certificado->aviso_vencimento_enviado = 1;
                $certificado->data_aviso_vencimento_enviado = $request->get('data_aviso_vencimento_enviado');
                $certificado->save();

                return response()->json(['success' => true, 'message' => 'E-mails de aviso de vencimento enviados com sucesso!']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => true, 'message' => 'Erro ao enviar os e-mails de aviso de vencimento dos certificados.']);
        }
    }

    public function updateDataEnvioVencimento(Request $request, $hash_id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'data_aviso_vencimento_enviado' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => true, 'message' => 'Preencha todos os campos corretamente.']);
            } else {
                $certificado = Certificado::where('hash_id', $hash_id)->first();
                $certificado->aviso_vencimento_enviado = 1;
                $certificado->data_aviso_vencimento_enviado = $request->get('data_aviso_vencimento_enviado');
                $certificado->save();

                return response()->json(['success' => true, 'message' => 'Data de envio atualizada com sucesso!']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => true, 'message' => 'Erro ao editar a data: ' . $e->getMessage()]);
        }
    }

    public function extrairMailing(Certificado $certificado)
    {
        $nomeRelatorio = 'Certificado_mailing_' . $certificado->hash_id;
        return Excel::download(new MailingExport($certificado->hash_id), $nomeRelatorio . '.xlsx');
    }
}
