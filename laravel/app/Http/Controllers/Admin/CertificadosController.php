<?php

namespace App\Http\Controllers\Admin;

use App\Exports\RelatoriosExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CertificadosRequest;
use App\Models\AlunoCertificado;
use App\Models\AreaAdmin;
use App\Models\Certificado;
use App\Models\Contato;
use App\Models\Curso;
use App\Models\Instituicao;
use App\Models\Instrutor;
use App\Notifications\AlunosCertificadosNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CertificadosController extends Controller
{
    private function createHashId()
    {
        $hashId = str_pad(random_int(1000, 9999), 4, 0, STR_PAD_LEFT);
        $check = Certificado::where('hash_id', $hashId)->get();
        if (count($check) > 0) {
            return $this->checkHashId();
        } else {
            return $hashId;
        }
    }

    public function index()
    {
        if (isset($_GET['data'])) {
            $data = $_GET['data'];
            $certificados = Certificado::join('cursos', 'cursos.id', '=', 'certificados.curso_id')
                ->select('certificados.*', 'cursos.titulo as curso')
                ->where('data_emissao', $data)
                ->orderBy('certificados.data_emissao', 'desc')->paginate(20);
        } else {
            $data = null;
            $certificados = Certificado::join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->select('certificados.*', 'cursos.titulo as curso')
            ->orderBy('certificados.data_emissao', 'desc')->paginate(20);
        }
        
        return view('admin.certificados.index', compact('certificados', 'data'));
    }

    public function create()
    {
        $cursos = Curso::orderBy('titulo', 'asc')->get();
        $instituicoes = Instituicao::orderBy('nome', 'asc')->get();
        $instrutores = Instrutor::orderBy('nome', 'asc')->get();

        return view('admin.certificados.form', compact('cursos', 'instituicoes', 'instrutores'));
    }

    public function store(CertificadosRequest $request)
    {
        try {
            $input = $request->all();
            $hash = $this->createHashId();
            $input['hash_id'] = $hash;
            $input['curso_id'] = Curso::where('hash_id', $request->curso_id)->first()->id;
            $input['instituicao_id'] = Instituicao::where('hash_id', $request->instituicao_id)->first()->id;
            $input['instrutor_id'] = Instrutor::where('hash_id', $request->instrutor_id)->first()->id;
            if ($request->instituicao_parceira == "on") {
                $input['instituicao_parceira'] = true;
                $input['nome_instituicao'] = $request->nome_instituicao;
                $input['email_instituicao'] = $request->email_instituicao;
                $input['texto_instituicao'] = $request->texto_instituicao;
            }

            $certificado = Certificado::create($input);

            return redirect()->route('admin.certificados.edit', $certificado->hash_id)->with('certificado', $certificado);
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar certificado: ' . $e->getMessage()]);
        }
    }

    public function edit(Certificado $certificado)
    {
        $cursos = Curso::orderBy('titulo', 'asc')->get();
        $instituicoes = Instituicao::orderBy('nome', 'asc')->get();
        $instrutores = Instrutor::orderBy('nome', 'asc')->get();

        $alunosCertificados = AlunoCertificado::where('certificado_id', $certificado->id)
            ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->select('alunos.nome', 'alunos.email', 'alunos.hash_id', 'alunos_certificados.id as id_ac', 'alunos_certificados.certificado_emitido', 'alunos_certificados.data_envio_certificado', 'alunos_certificados.nota_prova_teorica', 'alunos_certificados.nota_prova_pratica', 'alunos_certificados.nota_final')
            ->orderBy('alunos.nome', 'asc')->get();
        // dd($alunosCertificados);

        return view('admin.certificados.form', compact('certificado', 'alunosCertificados', 'cursos', 'instituicoes', 'instrutores'));
    }

    public function update(CertificadosRequest $request, Certificado $certificado)
    {
        try {
            $input = $request->all();
            $input['curso_id'] = Curso::where('hash_id', $request->curso_id)->first()->id;
            $input['instituicao_id'] = Instituicao::where('hash_id', $request->instituicao_id)->first()->id;
            $input['instrutor_id'] = Instrutor::where('hash_id', $request->instrutor_id)->first()->id;
            if ($request->instituicao_parceira == "on") {
                $input['instituicao_parceira'] = true;
                $input['nome_instituicao'] = $request->nome_instituicao;
                $input['email_instituicao'] = $request->email_instituicao;
                $input['texto_instituicao'] = $request->texto_instituicao;
            }

            $alunosCertificados = AlunoCertificado::where('certificado_id', $certificado->id)->get();
            $countEmitidos = 0;
            foreach ($alunosCertificados as $aluno) {
                if ($aluno->certificado_emitido == 1) {
                    $countEmitidos++;
                }
            }

            if ($countEmitidos == count($alunosCertificados) && count($alunosCertificados) > 0) {
                $input['status_emails'] = "enviados";
            } elseif ($countEmitidos == 0 || count($alunosCertificados) == 0) {
                $input['status_emails'] = "nao_enviados";
            } else {
                $input['status_emails'] = "emails_pendentes";
            }

            $certificado->update($input);

            return redirect()->route('admin.certificados.index')->with('success', 'Certificado atualizado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao atualizar certificado: ' . $e->getMessage()]);
        }
    }

    public function sendEmails(Certificado $certificado)
    {
        try {
            $alunosCertificados = AlunoCertificado::where('certificado_id', $certificado->id)->where('certificado_emitido', 0);

            if (count($alunosCertificados->get()) > 0) {

                $alunosEmails = $alunosCertificados->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
                    ->join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
                    ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
                    ->select('alunos.*', 'cursos.titulo as curso', 'alunos_certificados.hash_id as ac_hash_id', 'alunos_certificados.certificado_emitido as certificado_emitido', 'alunos_certificados.certificado_emitido', 'certificados.instituicao_parceira', 'certificados.nome_instituicao', 'certificados.email_instituicao', 'certificados.texto_instituicao')->get();

                $textoEmail = AreaAdmin::first()->texto_email_certificado;

                foreach ($alunosEmails as $aluno) {
                    $data = [
                        'textoEmail' => $textoEmail,
                        'aluno'  => $aluno,
                    ];

                    if ($aluno->instituicao_parceira == 1) {
                        Mail::send('emails.certificados-instituicoes', $data, function ($m) use ($data) {
                            $m->to($data['aluno']->email, $data['aluno']->nome)
                                ->replyTo($data['aluno']->email_instituicao)
                                ->subject('[CERTIFICADO | Curso: ' . $data['aluno']->curso . '] ' . $data['aluno']->nome_instituicao);
                        });
                    } else {
                        Mail::send('emails.certificados-sbc', $data, function ($m) use ($data) {
                            $m->to($data['aluno']->email, $data['aluno']->nome)
                                ->subject('[CERTIFICADO | Curso: ' . $data['aluno']->curso . '] ' . config('app.name'));
                        });
                    }
                }

                $dataSBC = [
                    'email'  => Contato::first()->email,
                    'data'   => date('d/m/Y H:i:s'),
                    'alunos' => $alunosEmails,
                ];

                Mail::send('emails.certificados-resumo', $dataSBC, function ($m) use ($dataSBC) {
                    $m->to($dataSBC['email'], config('app.name'))
                        ->bcc('laura@trupe.net')
                        ->subject('[CERTIFICADO | Resumo Certificado Emitido] ' . config('app.name'));
                });

                foreach (AlunoCertificado::where('certificado_id', $certificado->id)->where('certificado_emitido', 0)->get() as $ac) {
                    $imgArCode = $ac->hash_id . time() . '.svg';
                    QrCode::size(120)->generate(route('consulta.show', $ac->hash_id), public_path() . '/assets/img/qr-code/' . $imgArCode);
                    $ac->qr_code = $imgArCode;
                    $ac->certificado_emitido = 1;
                    $ac->data_envio_certificado = date('Y-m-d H:i:s');
                    $ac->save();
                }

                $certificado->status_emails = "enviados";
                $certificado->data_status_emails_enviados = date('Y-m-d H:i:s');
                $certificado->save();


                return response()->json(['success' => true, 'message' => 'E-mails enviados com sucesso!']);
            } else {
                DB::rollBack();
                return response()->json(['error' => true, 'message' => 'Não existem alunos com envio de e-mail pendente neste certificado.']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => true, 'message' => 'Erro ao enviar e-mails e atualizar certificado emitido']);
        }
    }

    public function extrairRelatorio(Certificado $certificado)
    {
        $nomeRelatorio = 'Certificado_relatorio_' . $certificado->hash_id;
        return Excel::download(new RelatoriosExport($certificado->hash_id), $nomeRelatorio . '.xlsx');
    }
}
