<?php

namespace App\Http\Controllers\Admin;

use App\Exports\RelatoriosFiltrosExport;
use App\Http\Controllers\Controller;
use App\Models\AlunoCertificado;
use App\Models\Certificado;
use App\Models\Curso;
use App\Models\Instituicao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class RelatoriosController extends Controller
{
    public function index()
    {
        $cursos = Curso::orderBy('titulo', 'asc')->get();
        $instituicoes = Instituicao::orderBy('nome', 'asc')->get();

        return view('admin.relatorios', compact('cursos', 'instituicoes'));
    }

    public function postFiltros(Request $request)
    {
        $filtro = $request->get('filtro');
        $dataInicio = $request->get('data_inicio');
        $dataFim = $request->get('data_fim');
        $cursoHashId = $request->get('curso_id');
        $instituicaoHashId = $request->get('instituicao_id');

        $certificados = Certificado::whereBetween('data_emissao', [$dataInicio, $dataFim])
            ->join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->join('instituicoes', 'instituicoes.id', '=', 'certificados.instituicao_id')
            ->where('alunos_certificados.certificado_emitido', 1)
            ->select('certificados.id', 'certificados.hash_id', 'certificados.data_emissao', 'cursos.hash_id as curso_hash_id', 'cursos.titulo as curso_nome', 'instituicoes.hash_id as instituicao_hash_id', 'instituicoes.nome as instituicao_nome', 'certificados.cidade', 'certificados.uf', DB::raw('count(alunos_certificados.hash_id) as totalAlunos'))
            ->groupBy('certificados.id', 'certificados.hash_id', 'certificados.data_emissao', 'cursos.hash_id', 'cursos.titulo', 'instituicoes.hash_id', 'instituicoes.nome', 'certificados.cidade', 'certificados.uf')
            ->orderBy('cursos.titulo');

        if ($filtro == 'cursos' && !empty($cursoHashId)) {
            $certificados = $certificados->where('cursos.hash_id', $cursoHashId)->get();
        } elseif ($filtro == 'instituicoes' && !empty($instituicaoHashId)) {
            $certificados = $certificados->where('instituicoes.hash_id', $instituicaoHashId)->get();
        } else {
            $certificados = $certificados->get();
        }

        $totalCertificados = 0;
        $totalAlunos = 0;
        foreach ($certificados as $certificado) {
            $totalCertificados++;
            $totalAlunos = $totalAlunos + $certificado->totalAlunos;
        }

        $cursos = Curso::orderBy('titulo', 'asc')->get();
        $instituicoes = Instituicao::orderBy('nome', 'asc')->get();

        return view('admin.relatorios', compact('filtro', 'dataInicio', 'dataFim', 'certificados', 'totalCertificados', 'totalAlunos', 'cursos', 'instituicoes', 'cursoHashId', 'instituicaoHashId'))->with('relatorioFiltro', true);
    }

    public function getTotais(Request $request)
    {
        try {
            $dataInicio = $request->get('data_inicio');
            $dataFim = $request->get('data_fim');

            $totalCertificados = count(Certificado::whereBetween('data_emissao', [$dataInicio, $dataFim])
                ->join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
                ->where('alunos_certificados.certificado_emitido', 1)
                ->select('certificados.*')
                ->distinct()->get());

            $totalAlunos = count(AlunoCertificado::join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
                ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
                ->whereBetween('certificados.data_emissao', [$dataInicio, $dataFim])
                ->where('alunos_certificados.certificado_emitido', 1)
                ->select('alunos.*')
                ->distinct()->get());

            $totalInstituicoes = count(Certificado::whereBetween('data_emissao', [$dataInicio, $dataFim])
                ->join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
                ->join('instituicoes', 'instituicoes.id', '=', 'certificados.instituicao_id')
                ->where('alunos_certificados.certificado_emitido', 1)
                ->select('instituicoes.*')
                ->distinct()->get());

            $totalVencimentos = count(Certificado::whereBetween('data_validade', [$dataInicio, $dataFim])
                ->join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
                ->where('alunos_certificados.certificado_emitido', 1)
                ->select('certificados.*')
                ->distinct()->get());

            $mediaAlunos = number_format($totalAlunos / $totalCertificados, 2, ",", ".");

            $cursos = Curso::orderBy('titulo', 'asc')->get();
            $instituicoes = Instituicao::orderBy('nome', 'asc')->get();

            return response()->json(['success' => true, 'dataInicio' => strftime("%d/%m/%Y", strtotime($dataInicio)), 'dataFim' => strftime("%d/%m/%Y", strtotime($dataFim)), 'totalCertificados' => $totalCertificados, 'totalAlunos' => $totalAlunos, 'totalInstituicoes' => $totalInstituicoes, 'totalVencimentos' => $totalVencimentos, 'mediaAlunos' => $mediaAlunos, 'cursos' => $cursos, 'instituicoes' => $instituicoes]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Erro ao adicionar o aluno neste certificado.']);
        }
    }

    public function extrairRelatorio($dataInicio, $dataFim, $filtro)
    {
        if ($filtro == "cursos" && $_GET['cursoHashId'] != null) {
            $nomeRelatorio = 'Relatório_Cursos_' . $_GET['cursoHashId'] . '_' . $dataInicio . '_' . $dataFim;
        } elseif ($filtro == "instituicoes" && $_GET['instituicaoHashId'] != null) {
            $nomeRelatorio = 'Relatório_Instituições_' . $_GET['instituicaoHashId'] . '_' . $dataInicio . '_' . $dataFim;
        } else {
            $nomeRelatorio = 'Relatório_' . $dataInicio . '_' . $dataFim;
        }
        return Excel::download(new RelatoriosFiltrosExport($dataInicio, $dataFim, $filtro, $_GET['cursoHashId'], $_GET['instituicaoHashId']), $nomeRelatorio . '.xlsx');
    }
}
