<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Aluno;
use App\Models\AlunoCertificado;
use App\Models\Certificado;
use App\Models\Curso;
use App\Models\Instituicao;
use App\Models\Instrutor;
use Illuminate\Http\Request;

class BuscaController extends Controller
{
    public function search(Request $request)
    {
        $termo = $request->busca;
        $resultados = [];

        if (!empty($termo)) {

            $buscaAlunos = Aluno::busca($termo)->get();
            foreach ($buscaAlunos as $dados) {
                $resultados['alunos'][$dados->id]['hash_id'] = $dados->hash_id;
                $resultados['alunos'][$dados->id]['link']   = route('admin.alunos.edit', $dados->hash_id);
                $resultados['alunos'][$dados->id]['menu']   = "ALUNOS";
                $resultados['alunos'][$dados->id]['titulo'] = 'Nome: ' . $dados->nome;
            }

            $buscaCursos = Curso::busca($termo)->get();
            foreach ($buscaCursos as $dados) {
                $resultados['cursos'][$dados->id]['hash_id'] = $dados->hash_id;
                $resultados['cursos'][$dados->id]['link']   = route('admin.cursos.edit', $dados->hash_id);
                $resultados['cursos'][$dados->id]['menu']   = "CURSOS";
                $resultados['cursos'][$dados->id]['titulo'] = 'Título: ' . $dados->titulo;
            }

            $buscaInstituicoes = Instituicao::busca($termo)->get();
            foreach ($buscaInstituicoes as $dados) {
                $resultados['instituicoes'][$dados->id]['hash_id'] = $dados->hash_id;
                $resultados['instituicoes'][$dados->id]['link']   = route('admin.instituicoes.edit', $dados->hash_id);
                $resultados['instituicoes'][$dados->id]['menu']   = "INSTITUIÇÕES";
                $resultados['instituicoes'][$dados->id]['titulo'] = 'Nome: ' . $dados->nome;
            }

            $buscaInstrutores = Instrutor::busca($termo)->get();
            foreach ($buscaInstrutores as $dados) {
                $resultados['instrutores'][$dados->id]['hash_id'] = $dados->hash_id;
                $resultados['instrutores'][$dados->id]['link']   = route('admin.instrutores.edit', $dados->hash_id);
                $resultados['instrutores'][$dados->id]['menu']   = "INSTRUTORES";
                $resultados['instrutores'][$dados->id]['titulo'] = 'Nome: ' . $dados->nome;
            }

            $buscaCertificados = Certificado::join('cursos', 'cursos.id', '=', 'certificados.curso_id')
                ->join('instituicoes', 'instituicoes.id', '=', 'certificados.instituicao_id')
                ->select('certificados.hash_id as certificado_hash_id', 'cursos.hash_id as curso_hash_id', 'cursos.titulo as curso_nome', 'instituicoes.nome as instituicao_nome')
                ->busca($termo)->get();
            foreach ($buscaCertificados as $dados) {
                $resultados['certificados'][$dados->id]['hash_id'] = $dados->hash_id;
                $resultados['certificados'][$dados->id]['link']   = route('admin.certificados.edit', $dados->certificado_hash_id);
                $resultados['certificados'][$dados->id]['menu']   = "CERTIFICADOS";
                $resultados['certificados'][$dados->id]['titulo'] = 'Curso: ' . $dados->curso_nome . " - Instituição: " . $dados->instituicao_nome;
            }

            $buscaAlunosCertificados = AlunoCertificado::join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
                ->join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
                ->select('alunos_certificados.hash_id', 'alunos.hash_id as aluno_hash_id', 'alunos.nome as aluno_nome', 'certificados.hash_id as certificado_hash_id')
                ->busca($termo)->get();
            foreach ($buscaAlunosCertificados as $dados) {
                $resultados['alunosCertificados'][$dados->id]['hash_id'] = $dados->hash_id;
                $resultados['alunosCertificados'][$dados->id]['link']   = route('admin.alunos.show', $dados->aluno_hash_id);
                $resultados['alunosCertificados'][$dados->id]['menu']   = "ALUNOS CERTIFICADOS";
                $resultados['alunosCertificados'][$dados->id]['titulo'] = 'Aluno: ' . $dados->aluno_nome . " - Certificado: " . $dados->hash_id;
            }
        } else {
            $resultados = [];
        }

        // dd($resultados);

        return view('admin.busca', compact('termo', 'resultados'));
    }
}
