<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AlunosRequest;
use App\Models\Aluno;
use App\Models\AlunoCertificado;
use App\Models\AlunosImport as ModelsAlunosImport;
use App\Models\AreaAdmin;
use App\Models\Certificado;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AlunosController extends Controller
{
    private function createHashId()
    {
        $hashId = str_pad(random_int(100000, 999999), 6, 0, STR_PAD_LEFT);
        $check = Aluno::where('hash_id', $hashId)->get();
        if (count($check) > 0) {
            return $this->createHashId();
        } else {
            return $hashId;
        }
    }

    public function index()
    {
        $alunos = Aluno::orderBy('nome', 'asc')->paginate(20);
        $certificados = Certificado::join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->select('certificados.*', 'alunos_certificados.aluno_id as aluno_id', 'alunos_certificados.hash_id as hash_aluno_certif', 'cursos.titulo as curso')
            ->where('alunos_certificados.certificado_emitido', 1)
            ->orderBy('alunos_certificados.created_at')->get();

        return view('admin.alunos.index', compact('alunos', 'certificados'));
    }

    public function create()
    {
        return view('admin.alunos.form');
    }

    public function store(AlunosRequest $request)
    {
        try {
            $input = $request->all();
            $input['password'] = null;
            $hash = $this->createHashId();
            $input['hash_id'] = $hash;

            Aluno::create($input);

            return redirect()->route('admin.alunos.index')->with('success', 'Aluno adicionado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar aluno: ' . $e->getMessage()]);
        }
    }

    public function show(Aluno $aluno)
    {
        $certificados = Certificado::join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
            ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->join('instrutores', 'instrutores.id', '=', 'certificados.instrutor_id')
            ->join('instituicoes', 'instituicoes.id', '=', 'certificados.instituicao_id')
            ->select('alunos.nome as aluno_nome', 'certificados.hash_id', 'certificados.data_emissao', 'certificados.data_validade', 'certificados.cidade', 'certificados.uf', 'alunos_certificados.hash_id as ac_hash_id', 'cursos.titulo as curso_titulo', 'cursos.imagem as curso_imagem', 'instrutores.nome as instrutor_nome', 'instrutores.hash_id as instrutor_id', 'instituicoes.nome as instituicao_nome', 'alunos_certificados.qr_code')
            ->where('alunos_certificados.aluno_id', $aluno->id)
            ->where('alunos_certificados.certificado_emitido', 1)
            ->orderBy('alunos_certificados.created_at')->get();

        return view('admin.alunos.show', compact('aluno', 'certificados'));
    }

    public function reenviarCertificado(Aluno $aluno, $hashId)
    {
        try {
            $alunoCertificado = AlunoCertificado::join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
                ->join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
                ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
                ->select('alunos.*', 'cursos.titulo as curso', 'alunos_certificados.hash_id as ac_hash_id', 'alunos_certificados.certificado_emitido as ac_hash_id', 'alunos_certificados.certificado_emitido', 'certificados.instituicao_parceira', 'certificados.nome_instituicao', 'certificados.email_instituicao', 'certificados.texto_instituicao')
                ->where('certificados.hash_id', $hashId)
                ->where('alunos_certificados.aluno_id', $aluno->id)
                ->first();

            $textoEmail = AreaAdmin::first()->texto_email_certificado;

            $data = [
                'textoEmail' => $textoEmail,
                'aluno'  => $alunoCertificado,
            ];

            if ($alunoCertificado->instituicao_parceira == 1) {
                Mail::send('emails.certificados-instituicoes', $data, function ($m) use ($data) {
                    $m->to($data['aluno']->email, $data['aluno']->nome)
                        ->replyTo($data['aluno']->email_instituicao)
                        ->subject('[CERTIFICADO | Curso: ' . $data['aluno']->curso . '] ' . $data['aluno']->nome_instituicao);
                });
            } else {
                Mail::send('emails.certificados-sbc', $data, function ($m) use ($data) {
                    $m->to($data['aluno']->email, $data['aluno']->nome)
                        ->subject('[CERTIFICADO | Curso: ' . $data['aluno']->curso . '] ' . config('app.name'));
                });
            }

            $alunoCertificado->certificado_emitido = 1;
            $alunoCertificado->data_envio_certificado = date('Y-m-d H:i:s');
            $alunoCertificado->save();

            return response()->json(['success' => true, 'message' => 'Certificado reenviado com sucesso!']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => true, 'message' => 'Erro ao reenviar certificado.']);
        }
    }

    public function edit(Aluno $aluno)
    {
        return view('admin.alunos.form', compact('aluno'));
    }

    public function update(AlunosRequest $request, Aluno $aluno)
    {
        try {
            $input = $request->all();
            if (isset($aluno->password)) {
                $input['password'] = $aluno->password;
            } else {
                $input['password'] = null;
            }

            $aluno->update($input);

            return redirect()->route('admin.alunos.index')->with('success', 'Aluno atualizado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao editar aluno: ' . $e->getMessage()]);
        }
    }

    public function delete(Aluno $aluno)
    {
        try {
            $aluno->delete();

            return redirect()->route('admin.alunos.index')->with('success', 'Aluno excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir aluno: ' . $e->getMessage()]);
        }
    }

    public function importExcel(Request $request)
    {
        if (!$request->hasFile('excel_alunos')) {
            return back()->withErrors(['Erro ao importar alunos: Arquivo não encontrado.']);
        }

        try {
            $arquivo = $request->file('excel_alunos');
            $import = new ModelsAlunosImport($arquivo);

            $importado = $import->importar();
            $adicionados = $importado['alunos'];
            $repetidos = $importado['repetidos'];
            $erros = $importado['erros'];

            return back()->with(['success' => count($adicionados) > 0 ? 'Alunos novos inseridos com sucesso!' : 'Não existem novos alunos.', 'repetidos' => $repetidos, 'erros' => $erros]);
        } catch (Exception $e) {
            dd($e);
            return back()->withErrors(['Erro ao importar alunos: ' . $e->getMessage()]);
        }
    }
}
