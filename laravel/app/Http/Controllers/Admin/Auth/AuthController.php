<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function index()
    {
        return view('admin.auth.login');
    }

    protected function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        if (Auth::guard('admin')->attempt([
            'email'    => $request->email,
            'password' => $request->password
        ])) {
            return redirect()->route('admin.home');
        } else {
            return back()->withErrors(['Erro ao logar: e-mail ou senha inválidos']);
        }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect(RouteServiceProvider::HOME);
    }

    public function edit(Admin $admin)
    {
        Auth::guard('admin')->check();
        return view('admin.auth.editar-cadastro', compact('admin'));
    }

    public function update(Request $request, Admin $admin)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'nome'  => 'required',
            'email' => 'required|email|exists:admins',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            $data['password'] = Hash::make($request->password);
            $admin->update($data);

            return redirect()->route('admin.edit', $admin->id)->with('success', 'Cadastro editado com sucesso!');
        }
    }
}
