<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Contato;
use App\Notifications\AdminResetPasswordNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

# Include the Autoloader (see "Libraries" for install instructions)
// require 'vendor/autoload.php';
require_once __DIR__ . '/../../../../../vendor/autoload.php';

use Mailgun\Mailgun;

class PasswordController extends Controller
{
    public function index()
    {
        return view('admin.auth.esqueci-a-senha');
    }

    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $admin = Admin::where('email', $request->email)->first();

        if (empty($admin)) {
            return back()->withErrors(['Erro ao soliticar a redefinição de senha: cadastro não encontrado']);
        } else {
            $token = Str::random(64);

            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);

            $data = [
                'token' => $token,
                'email' => $admin->email,
                'nome'  => $admin->nome,
            ];

            Mail::send('emails.admin-password', $data, function ($m) use ($data) {
                $m->to($data['email'], $data['nome'])
                    ->subject('[RECUPERAÇÃO DE SENHA] ' . config('app.name'));
            });

            return back()->with('success', 'Enviamos o link de redefinição de senha por e-mail!');
        }
    }

    public function indexReset($token)
    {
        $email = $_GET['email'];

        return view('admin.auth.recuperar-senha', ['token' => $token, 'email' => $email]);
    }

    public function postReset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:admins',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $updatePassword = DB::table('password_resets')
            ->where([
                'email' => $request->email,
                'token' => $request->token
            ])->first();

        if (!$updatePassword) {
            return back()->withErrors(['Erro ao redefinir senha: token inválido']);
        }

        $admin = Admin::where('email', $request->email)->update(['password' => Hash::make($request->password)]);

        DB::table('password_resets')->where(['email' => $request->email])->delete();

        return redirect()->route('admin.login')->with('success', 'Sua senha foi alterada com sucesso! Faça seu login');
    }
}
