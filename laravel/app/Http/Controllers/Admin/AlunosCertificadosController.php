<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AlunosCertificadosRequest;
use App\Models\Aluno;
use App\Models\AlunoCertificado;
use App\Models\Certificado;
use Illuminate\Http\Request;

class AlunosCertificadosController extends Controller
{
    public function findAluno($cpf)
    {
        $aluno = Aluno::where('cpf', $cpf)->first();
        if (isset($aluno)) {
            return response()->json(['aluno' => $aluno]);
        } else {
            return response()->json(['erro' => 'CPF não encontrado.']);
        }
    }

    public function addAlunoCertificado(AlunosCertificadosRequest $request)
    {
        try {
            $input = $request->all();
            $input['hash_id'] = $input['certificado_id'] . $input['aluno_id'];
            $certificado = Certificado::where('hash_id', $request->certificado_id)->first();
            $input['certificado_id'] = $certificado->id;;
            $input['aluno_id'] = Aluno::where('hash_id', $request->aluno_id)->first()->id;
            $input['certificado_emitido'] = 0;
            $input['data_envio_certificado'] = null;

            $check = AlunoCertificado::where('aluno_id', $input['aluno_id'])->where('certificado_id', $input['certificado_id'])->get();

            if (count($check) > 0) {
                $alunoCertificado = AlunoCertificado::where('aluno_id', $input['aluno_id'])->where('certificado_id', $input['certificado_id'])->first();

                $notas = [
                    'nota_prova_teorica' => $input['nota_prova_teorica'],
                    'nota_prova_pratica' => $input['nota_prova_pratica'],
                    'nota_final' => $input['nota_final'],
                ];

                $alunoCertificado->update($notas);

                $aluno = Aluno::where('id', $alunoCertificado->aluno_id)->first();

                return response()->json(['success' => true, 'message' => 'Notas atualizadas com sucesso!', 'aluno' => $aluno, 'alunoCertificado' => $alunoCertificado]);
                
            } else {
                $alunoCertificado = AlunoCertificado::create($input);

                $certificado->status_emails = "emails_pendentes";
                $certificado->save();

                $aluno = Aluno::where('id', $alunoCertificado->aluno_id)->first();

                return response()->json(['success' => true, 'message' => 'Aluno adicionado com sucesso!', 'aluno' => $aluno, 'alunoCertificado' => $alunoCertificado]);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Erro ao adicionar o aluno neste certificado.']);
        }
    }

    public function deleteAlunoCertificado($certificadoId, $alunoId)
    {
        try {
            $aluno = Aluno::where('hash_id', $alunoId)->first();
            $certificado = Certificado::where('hash_id', $certificadoId)->first();
            $check = AlunoCertificado::where('certificado_id', $certificado->id)->where('aluno_id', $aluno->id)->first();
            $check->delete();

            return response()->json(['success' => true, 'message' => 'Aluno removido com sucesso!', 'id' => $check->id]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => 'Erro ao remover o aluno deste certificado.']);
        }
    }
}
