<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\InstituicoesRequest;
use App\Models\Certificado;
use App\Models\Instituicao;
use Illuminate\Http\Request;

class InstituicoesController extends Controller
{
    private function createHashId()
    {
        $hashId = str_pad(random_int(1000, 9999), 4, 0, STR_PAD_LEFT);
        $check = Instituicao::where('hash_id', $hashId)->get();
        if (count($check) > 0) {
            return $this->checkHashId();
        } else {
            return $hashId;
        }
    }

    public function index()
    {
        $instituicoes = Instituicao::orderBy('nome', 'asc')->paginate(20);
        $certificados = Certificado::get();

        return view('admin.instituicoes.index', compact('instituicoes', 'certificados'));
    }

    public function create()
    {
        return view('admin.instituicoes.form');
    }

    public function store(InstituicoesRequest $request)
    {
        try {
            $input = $request->all();
            $hash = $this->createHashId();
            $input['hash_id'] = $hash;

            Instituicao::create($input);

            return redirect()->route('admin.instituicoes.index')->with('success', 'Instituição adicionado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar instituição: ' . $e->getMessage()]);
        }
    }

    public function edit(Instituicao $instituicao)
    {
        return view('admin.instituicoes.form', compact('instituicao'));
    }

    public function update(InstituicoesRequest $request, Instituicao $instituicao)
    {
        try {
            $input = $request->all();
            if (isset($instituicao->password)) {
                $input['password'] = $instituicao->password;
            } else {
                $input['password'] = null;
            }

            $instituicao->update($input);

            return redirect()->route('admin.instituicoes.index')->with('success', 'Instituição atualizado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao editar instituição: ' . $e->getMessage()]);
        }
    }

    public function delete(Instituicao $instituicao)
    {
        try {
            // incluir conferência se instituição já está em algum certificado com status de email enviado
            $instituicao->delete();

            return redirect()->route('admin.instituicoes.index')->with('success', 'Instituição excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir instituição: ' . $e->getMessage()]);
        }
    }
}
