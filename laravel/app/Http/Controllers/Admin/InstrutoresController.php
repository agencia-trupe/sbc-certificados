<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\InstrutoresRequest;
use App\Models\Certificado;
use App\Models\Instrutor;
use Illuminate\Http\Request;

class InstrutoresController extends Controller
{
    private function createHashId()
    {
        $hashId = 'ID'.str_pad(random_int(1000, 9999), 4, 0, STR_PAD_LEFT);
        $check = Instrutor::where('hash_id', $hashId)->get();
        if (count($check) > 0) {
            return $this->checkHashId();
        } else {
            return $hashId;
        }
    }

    public function index()
    {
        $instrutores = Instrutor::orderBy('nome', 'asc')->paginate(20);
        $certificados = Certificado::get();

        return view('admin.instrutores.index', compact('instrutores', 'certificados'));
    }

    public function create()
    {
        return view('admin.instrutores.form');
    }

    public function store(InstrutoresRequest $request)
    {
        try {
            $input = $request->all();
            $hash = $this->createHashId();
            $input['hash_id'] = $hash;

            Instrutor::create($input);

            return redirect()->route('admin.instrutores.index')->with('success', 'Instrutor adicionado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar instrutor: ' . $e->getMessage()]);
        }
    }

    public function edit(Instrutor $instrutor)
    {
        return view('admin.instrutores.form', compact('instrutor'));
    }

    public function update(InstrutoresRequest $request, Instrutor $instrutor)
    {
        try {
            $input = $request->all();
            if (isset($instrutor->password)) {
                $input['password'] = $instrutor->password;
            } else {
                $input['password'] = null;
            }

            $instrutor->update($input);

            return redirect()->route('admin.instrutores.index')->with('success', 'Instrutor atualizado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao editar instrutor: ' . $e->getMessage()]);
        }
    }

    public function delete(Instrutor $instrutor)
    {
        try {
            $instrutor->delete();

            return redirect()->route('admin.instrutores.index')->with('success', 'Instrutor excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir instrutor: ' . $e->getMessage()]);
        }
    }
}
