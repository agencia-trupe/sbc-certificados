<?php

namespace App\Http\Controllers\Profissionais\Auth;

use App\Http\Controllers\Controller;
use App\Models\Aluno;
use App\Notifications\AlunoResetPasswordNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PasswordController extends Controller
{
    public function index()
    {
        return view('profissionais.auth.esqueci-a-senha');
    }

    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $aluno = Aluno::where('email', $request->email)->first();

        if (empty($aluno)) {
            return back()->withErrors(['Erro ao soliticar a redefinição de senha: cadastro não encontrado']);
        } elseif ($aluno->ativo == 0) {
            return back()->withErrors(['Cadastro não está ativo, faça seu primeiro cadastro.']);
        } else {
            $token = Str::random(64);

            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);

            $data = [
                'token' => $token,
                'email' => $aluno->email,
                'nome'  => $aluno->nome,
            ];

            Mail::send('emails.profissionais-password', $data, function ($m) use ($data) {
                $m->to($data['email'], $data['nome'])
                    ->bcc('laura@trupe.net')
                    ->subject('[RECUPERAÇÃO DE SENHA] ' . config('app.name'));
            });

            return back()->with('success', 'Enviamos o link de redefinição de senha por e-mail!');
        }
    }

    public function indexReset($token)
    {
        $email = $_GET['email'];

        return view('profissionais.auth.recuperar-senha', ['token' => $token, 'email' => $email]);
    }

    public function postReset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:alunos',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $updatePassword = DB::table('password_resets')
            ->where([
                'email' => $request->email,
                'token' => $request->token
            ])->first();

        if (!$updatePassword) {
            return back()->withErrors(['Erro ao redefinir senha: token inválido']);
        }

        $aluno = Aluno::where('email', $request->email)->update(['password' => Hash::make($request->password)]);

        DB::table('password_resets')->where(['email' => $request->email])->delete();

        return redirect()->route('profissionais.login')->with('success', 'Sua senha foi alterada com sucesso! Faça seu login');
    }
}
