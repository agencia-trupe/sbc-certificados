<?php

namespace App\Http\Controllers\Profissionais\Auth;

use App\Http\Controllers\Controller;
use App\Models\Aluno;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function index()
    {
        return view('profissionais.auth.login');
    }

    protected function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        if (Auth::guard('profissionais')->attempt([
            'email'    => $request->email,
            'password' => $request->password
        ])) {
            return redirect()->route('profissionais.certificados');
        } else {
            return back()->withErrors(['Erro ao logar: e-mail ou senha inválidos']);
        }
    }

    public function logout()
    {
        Auth::guard('profissionais')->logout();
        return redirect(RouteServiceProvider::PROFISSIONAIS);
    }

    public function indexCadastro()
    {
        return view('profissionais.auth.cadastro');
    }

    protected function cadastro(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $aluno = Aluno::where('email', $request->email)->first();

        if (empty($aluno)) {
            return back()->withErrors(['Erro ao fazer primeiro cadastro: e-mail não encontrado.']);
        } else {
            $aluno->password = Hash::make($request->password);
            $aluno->ativo = 1;
            $aluno->save();
        }

        if (Auth::guard('profissionais')->attempt([
            'email'    => $aluno->email,
            'password' => $request->password
        ])) {
            return redirect()->route('profissionais.certificados');
        } else {
            return back()->withErrors(['Erro ao logar: e-mail ou senha inválidos']);
        }
    }

    public function edit(Aluno $aluno)
    {
        Auth::guard('profissionais')->check();
        return view('profissionais.auth.editar-cadastro', compact('aluno'));
    }

    public function update(Request $request, Aluno $aluno)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            $data['password'] = Hash::make($request->password);
            $aluno->update($data);

            return redirect()->route('profissionais.edit', $aluno->id)->with('success', 'Senha atualizada com sucesso!');
        }
    }
}
