<?php

namespace App\Http\Controllers\Profissionais;

use App\Http\Controllers\Controller;
use App\Models\AlunoCertificado;
use App\Models\AreaAdmin;
use App\Models\Certificado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CertificadosController extends Controller
{
    public function index()
    {
        $aluno = Auth::guard('profissionais')->user();

        $tempoVencimento = AreaAdmin::first()->tempo_aviso_vencimento;
        $dataAtual = date('Y-m-d');
        $dataAvisoVencimento = date('Y-m-d', strtotime("+6 months", strtotime($dataAtual)));

        $certificadosValidos = Certificado::join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
            ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->select('alunos_certificados.hash_id as ac_hash_id', 'cursos.titulo as curso', 'certificados.hash_id')
            ->where('alunos.id', $aluno->id)
            ->where('alunos_certificados.certificado_emitido', 1)
            ->where('certificados.data_validade', '>', $dataAvisoVencimento)
            ->orderBy('certificados.data_emissao', 'desc')
            ->get();

        $certificadosRenovacao = Certificado::join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
            ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->select('alunos_certificados.hash_id as ac_hash_id', 'cursos.titulo as curso', 'certificados.hash_id')
            ->where('alunos.id', $aluno->id)
            ->where('alunos_certificados.certificado_emitido', 1)
            ->whereBetween('certificados.data_validade', [$dataAtual, $dataAvisoVencimento])
            ->orderBy('certificados.data_emissao', 'desc')
            ->get();

        $certificadosVencidos = Certificado::join('alunos_certificados', 'alunos_certificados.certificado_id', '=', 'certificados.id')
            ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->select('alunos_certificados.hash_id as ac_hash_id', 'cursos.titulo as curso', 'certificados.hash_id')
            ->where('alunos.id', $aluno->id)
            ->where('alunos_certificados.certificado_emitido', 1)
            ->where('certificados.data_validade', '<=', $dataAtual)
            ->orderBy('certificados.data_emissao', 'desc')
            ->get();


        return view('profissionais.certificados', compact('certificadosValidos', 'certificadosRenovacao', 'certificadosVencidos'));
    }

    public function show(Certificado $certificado)
    {
        $aluno = Auth::guard('profissionais')->user();

        $alunoCertificado = AlunoCertificado::join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
            ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->join('instrutores', 'instrutores.id', '=', 'certificados.instrutor_id')
            ->join('instituicoes', 'instituicoes.id', '=', 'certificados.instituicao_id')
            ->select('alunos.nome as aluno_nome', 'certificados.hash_id', 'certificados.data_emissao', 'certificados.data_validade', 'certificados.cidade', 'certificados.uf', 'alunos_certificados.hash_id as ac_hash_id', 'cursos.titulo as curso_titulo', 'cursos.imagem as curso_imagem', 'instrutores.nome as instrutor_nome', 'instrutores.hash_id as instrutor_id', 'instituicoes.nome as instituicao_nome', 'alunos_certificados.qr_code')
            ->where('alunos_certificados.certificado_id', $certificado->id)
            ->where('alunos_certificados.aluno_id', $aluno->id)
            ->where('alunos_certificados.certificado_emitido', 1)
            ->first();

        return view('profissionais.certificados-show', compact('alunoCertificado'));
    }

    public function showPrint($hash_id)
    {
        $aluno = Auth::guard('profissionais')->user();

        $certificado = AlunoCertificado::join('certificados', 'certificados.id', '=', 'alunos_certificados.certificado_id')
            ->join('alunos', 'alunos.id', '=', 'alunos_certificados.aluno_id')
            ->join('cursos', 'cursos.id', '=', 'certificados.curso_id')
            ->join('instrutores', 'instrutores.id', '=', 'certificados.instrutor_id')
            ->join('instituicoes', 'instituicoes.id', '=', 'certificados.instituicao_id')
            ->select('alunos.nome as aluno_nome', 'certificados.hash_id', 'certificados.data_emissao', 'certificados.data_validade', 'certificados.cidade', 'certificados.uf', 'alunos_certificados.hash_id as ac_hash_id', 'cursos.titulo as curso_titulo', 'cursos.imagem as curso_imagem', 'instrutores.nome as instrutor_nome', 'instrutores.hash_id as instrutor_id', 'instituicoes.nome as instituicao_nome', 'alunos_certificados.qr_code')
            ->where('alunos_certificados.hash_id', $hash_id)
            ->where('alunos_certificados.aluno_id', $aluno->id)
            ->where('alunos_certificados.certificado_emitido', 1)
            ->first();

        if ($certificado->data_validade > date('Y-m-d')) {
            return view('profissionais.certificado-valido', compact('certificado'));
        }
        if ($certificado->data_validade <= date('Y-m-d')) {
            return view('profissionais.certificado-vencido', compact('certificado'));
        }
    }
}
