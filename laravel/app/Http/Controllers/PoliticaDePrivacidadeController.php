<?php

namespace App\Http\Controllers;

use App\Models\PoliticaDePrivacidade;
use Illuminate\Http\Request;

class PoliticaDePrivacidadeController extends Controller
{
    public function index()
    {
        $politica = PoliticaDePrivacidade::first();

        return view('politica-de-privacidade', compact('politica'));
    }
}
