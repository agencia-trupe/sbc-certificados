<?php

namespace App\Models;

use App\Notifications\AlunoResetPasswordNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Aluno extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 'alunos';

    protected $fillable = [
        'hash_id',
        'nome',
        'cpf',
        'email',
        'password',
        'profissao',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function certificados()
    {
        return $this->hasMany(AlunoCertificado::class, 'aluno_id');
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('cpf', 'LIKE', '%' . $termo . '%')
            ->orWhere('nome', 'LIKE', '%' . $termo . '%');
    }
}
