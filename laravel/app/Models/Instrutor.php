<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instrutor extends Model
{
    use HasFactory;

    protected $table = 'instrutores';

    protected $guarded = ['id'];

    public function certificado()
    {
        return $this->belongsTo(Certificado::class);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('hash_id', 'LIKE', '%' . $termo . '%')
            ->orWhere('nome', 'LIKE', '%' . $termo . '%');
    }
}
