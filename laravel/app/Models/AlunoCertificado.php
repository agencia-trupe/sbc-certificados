<?php

namespace App\Models;

use App\Notifications\AlunosCertificadosNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class AlunoCertificado extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'alunos_certificados';

    protected $guarded = ['id'];

    protected $dates = ['data_envio_certificado'];

    public function scopeCertificadoEmitido($query)
    {
        return $query->where('certificado_emitido', '=', 1);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('alunos_certificados.hash_id', 'LIKE', '%' . $termo . '%')
            ->orWhere('alunos.hash_id', 'LIKE', '%' . $termo . '%')
            ->orWhere('alunos.nome', 'LIKE', '%' . $termo . '%');
    }
}
