<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instituicao extends Model
{
    use HasFactory;

    protected $table = 'instituicoes';

    protected $guarded = ['id'];

    public function certificado()
    {
        return $this->belongsTo(Certificado::class);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('nome', 'LIKE', '%' . $termo . '%');
    }
}
