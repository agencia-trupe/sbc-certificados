<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Configuracao extends Model
{
    use HasFactory;

    protected $table = 'configuracoes';

    protected $guarded = ['id'];

    public static function upload_imagem_de_compartilhamento()
    {
        return CropImage::make('imagem_de_compartilhamento', [
            'width'  => null,
            'height' => null,
            'path'   => 'assets/img/'
        ]);
    }
}
