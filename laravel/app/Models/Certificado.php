<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificado extends Model
{
    use HasFactory;

    protected $table = 'certificados';

    protected $guarded = ['id'];

    protected $dates = ['data_emissao', 'data_validade', 'data_aviso_vencimento_enviado', 'data_status_emails_enviados'];

    protected $status = [
        'nao_enviados' => 'Nenhum e-mail enviado',
        'enviados' => 'E-mails enviados',
        'emails_pendentes' => 'Alguns e-mails pendentes'
    ];

    public function scopeAvisoVencimentoEnviado($query)
    {
        return $query->where('aviso_vencimento_enviado', '=', 1);
    }

    public function scopeInstituicaoParceira($query)
    {
        return $query->where('instituicao_parceira', '=', 1);
    }

    public function alunos()
    {
        return $this->hasMany(AlunoCertificado::class, 'certificado_id');
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('certificados.hash_id', 'LIKE', '%' . $termo . '%')
            ->orWhere('cursos.hash_id', 'LIKE', '%' . $termo . '%')
            ->orWhere('cursos.titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('instituicoes.nome', 'LIKE', '%' . $termo . '%');
    }
}
