<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;

    protected $table = 'cursos';

    protected $guarded = ['id'];

    public function certificado()
    {
        return $this->belongsTo(Certificado::class);
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('hash_id', 'LIKE', '%' . $termo . '%')
            ->orWhere('titulo', 'LIKE', '%' . $termo . '%');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 430,
            'height' => 90,
            'path'   => 'assets/img/cursos/'
        ]);
    }
}
