<?php

namespace App\Models;

use App\Imports\AlunosImport as ImportsAlunosImport;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class AlunosImport extends Model
{
    use HasFactory;

    private $arquivo;
    private $resultados;
    private $alunos;

    private $campos = [
        'profissao' => 'PROFISSÃO',
        'nome' => 'Nome',
        'cpf' => 'CPF,',
        'e_mail' => 'e-mail'
    ];

    public function __construct($arquivo)
    {
        $this->arquivo = $arquivo;
        $this->resultados = Excel::toCollection(new ImportsAlunosImport, $this->arquivo)->first();
    }

    public function importar()
    {
        $erros = $this->validarEmail();
        $repetidos = $this->validar();

        $alunos = $this->inserirAlunos();

        return ['alunos' => $alunos, 'repetidos' => $repetidos, 'erros' => $erros];
    }

    private function validarEmail()
    {
        $erros = [];

        foreach ($this->resultados as $rowNum => $row) {
            $checkEmail = filter_var($row['email'], FILTER_VALIDATE_EMAIL);

            if (!$checkEmail) {
                $erros[] = "O e-mail " . $row['email'] . " não é um e-mail válido.";
            }
        }
        return $erros;
    }

    private function validar()
    {
        $repetidos = [];
        $erros = [];

        foreach ($this->resultados as $rowNum => $row) {


            // remove pontos e traços
            $alunoCpf = str_replace(".", "", str_replace("-", "", $row['cpf']));
            // verifica se tem 11 caracteres, se não completa com "0" a esquerda
            $alunoCpf = str_pad($alunoCpf, 11, "0", STR_PAD_LEFT);

            // formata cpf 000.000.000-00
            if (strlen($alunoCpf)) {
                $alunoCpf = substr($alunoCpf, 0, 3) . '.' . substr($alunoCpf, 3, 3) . '.' . substr($alunoCpf, 6, 3) . '-' . substr($alunoCpf, 9);
            }

            $alunoEmail = strtolower(str_replace(" ", "", $row['email']));

            $checkAluno = Aluno::where('cpf', $alunoCpf)->orWhere('email', $alunoEmail)->first();

            if (isset($checkAluno)) {
                $repetidos[] = "O(A) aluno(a) " . $checkAluno->nome . " já possui cadastro.";
            } else {
                $repetidos = [];
            }
        }
        return $repetidos;
    }

    private function linhaVazia($row)
    {
        foreach ($this->campos as $campo => $titulo) {
            if ($row[$campo] != null) {
                return false;
            }
        }
        return true;
    }

    private function inserirAlunos()
    {
        $alunos = [];

        foreach ($this->resultados as $row) {

            if ($this->linhaVazia($row)) continue;

            // remove pontos e traços
            $alunoCpf = str_replace(".", "", str_replace("-", "", $row['cpf']));
            // verifica se tem 11 caracteres, se não completa com "0" a esquerda
            $alunoCpf = str_pad($alunoCpf, 11, "0", STR_PAD_LEFT);

            // formata cpf 000.000.000-00
            if (strlen($alunoCpf)) {
                $alunoCpf = substr($alunoCpf, 0, 3) . '.' . substr($alunoCpf, 3, 3) . '.' . substr($alunoCpf, 6, 3) . '-' . substr($alunoCpf, 9);
            }

            $alunoEmail = strtolower(str_replace(" ", "", $row['email']));

            $checkAluno = Aluno::where('cpf', $alunoCpf)->orWhere('email', $alunoEmail)->first();

            if (empty($checkAluno)) {
                $aluno = [
                    'hash_id' => $this->createHashId(),
                    'ativo' => 0,
                    'cpf' => $alunoCpf,
                    'nome'    => $row['nome'],
                    'profissao'     => $row['profissao'],
                    'email' => $alunoEmail,
                    'password' => null,
                    'created_at' => date('Y-m-d H:i')
                ];
                $alunos[] = Aluno::create($aluno);
            }
        }

        return $alunos;
    }

    private function createHashId()
    {
        $hashId = str_pad(random_int(100000, 999999), 6, 0, STR_PAD_LEFT);
        $check = Aluno::where('hash_id', $hashId)->get();
        if (count($check) > 0) {
            return $this->createHashId();
        } else {
            return $hashId;
        }
    }
}
