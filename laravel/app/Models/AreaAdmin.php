<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AreaAdmin extends Model
{
    use HasFactory;

    protected $table = 'area_admin';

    protected $guarded = ['id'];
}
