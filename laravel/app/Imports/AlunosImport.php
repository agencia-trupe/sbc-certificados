<?php

namespace App\Imports;

use App\Models\Aluno;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Throwable;

class AlunosImport implements ToCollection, WithStartRow, WithHeadingRow, WithMapping
{
    use Importable, SkipsErrors, SkipsFailures;

    public function startRow(): int
    {
        return 2;
    }

    public function map($row): array
    {
        return [
            'profissao' => $row['profissao'],
            'nome' => $row['nome'],
            'cpf' => $row['cpf'],
            'email' => $row['e_mail'],
        ];
    }

    public function collection(Collection $resultados)
    {
        //
    }

    public function headingRow(): int
    {
        return 1;
    }
}
