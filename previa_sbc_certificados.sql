--
-- Table structure for table `aceite_de_cookies`
--

DROP TABLE IF EXISTS `aceite_de_cookies`;
CREATE TABLE `aceite_de_cookies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `aceite_de_cookies`
--

LOCK TABLES `aceite_de_cookies` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
INSERT INTO `admins` VALUES (1,'admin','teste@trupe.net','$2y$10$8Jx66S6/Y2/iRH7IS4gHfOg62ssKpUY1ssNe92qJk09sqQ7LEkiuy',NULL,NULL,'2022-03-20 20:23:57');
UNLOCK TABLES;

--
-- Table structure for table `alunos`
--

DROP TABLE IF EXISTS `alunos`;
CREATE TABLE `alunos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hash_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT 0,
  `cpf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alunos_hash_id_unique` (`hash_id`),
  UNIQUE KEY `alunos_cpf_unique` (`cpf`),
  UNIQUE KEY `alunos_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alunos`
--

LOCK TABLES `alunos` WRITE;
UNLOCK TABLES;

-- ALTERAÇÃO 25/03/2022
alter table `alunos` add column `profissao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL after `nome`;

--
-- Table structure for table `area_admin`
--

DROP TABLE IF EXISTS `area_admin`;
CREATE TABLE `area_admin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tempo_aviso_vencimento` int(11) NOT NULL,
  `texto_email_vencimento` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto_email_certificado` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `area_admin`
--

LOCK TABLES `area_admin` WRITE;
INSERT INTO `area_admin` VALUES (1,6,'<p>(Texto vencimento) Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.&nbsp;</p>','<p>(Texto certificado emitido) Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',NULL,'2022-03-20 19:55:28');
UNLOCK TABLES;

--
-- Table structure for table `configuracoes`
--

DROP TABLE IF EXISTS `configuracoes`;
CREATE TABLE `configuracoes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `analytics_ua` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `analytics_g` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `codigo_gtm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pixel_facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configuracoes`
--

LOCK TABLES `configuracoes` WRITE;
INSERT INTO `configuracoes` VALUES (1,'SBC Certificados','','','','','','','',NULL,NULL);
UNLOCK TABLES;

--
-- Table structure for table `contatos`
--

DROP TABLE IF EXISTS `contatos`;
CREATE TABLE `contatos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contatos`
--

LOCK TABLES `contatos` WRITE;
INSERT INTO `contatos` VALUES (1,'contato@trupe.net',NULL,NULL);
UNLOCK TABLES;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
CREATE TABLE `cursos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hash_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cursos_hash_id_unique` (`hash_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cursos`
--

LOCK TABLES `cursos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `instituicoes`
--

DROP TABLE IF EXISTS `instituicoes`;
CREATE TABLE `instituicoes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hash_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `instituicoes_hash_id_unique` (`hash_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `instituicoes`
--

LOCK TABLES `instituicoes` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `instrutores`
--

DROP TABLE IF EXISTS `instrutores`;
CREATE TABLE `instrutores` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hash_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `instrutores_hash_id_unique` (`hash_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `instrutores`
--

LOCK TABLES `instrutores` WRITE;
UNLOCK TABLES;

-- ALTERAÇÃO 25/03/2022
alter table `instrutores` add column `cpf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL after `hash_id`;
alter table `instrutores` add column `email` varchar(255) COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL after `nome`;
alter table `instrutores` add column `profissao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL after `email`;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=323 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
INSERT INTO `migrations` VALUES (307,'2014_10_12_000000_create_users_table',1),(308,'2014_10_12_100000_create_password_resets_table',1),(309,'2019_08_19_000000_create_failed_jobs_table',1),(310,'2019_12_14_000001_create_personal_access_tokens_table',1),(311,'2022_03_07_221004_create_configuracoes_table',1),(312,'2022_03_07_222232_create_politica_de_privacidade_table',1),(313,'2022_03_07_223208_create_aceite_de_cookies_table',1),(314,'2022_03_07_223713_create_contatos_table',1),(315,'2022_03_07_224818_create_admins_table',1),(316,'2022_03_09_215026_create_area_admin_table',1),(317,'2022_03_10_135348_create_alunos_table',1),(318,'2022_03_13_200341_create_instrutores_table',1),(319,'2022_03_13_203921_create_instituicoes_table',1),(320,'2022_03_13_205615_create_cursos_table',1),(321,'2022_03_14_221012_create_certificados_table',1),(322,'2022_03_15_212508_create_alunos_certificados_table',1);
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `politica_de_privacidade`
--

DROP TABLE IF EXISTS `politica_de_privacidade`;
CREATE TABLE `politica_de_privacidade` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `politica_de_privacidade`
--

LOCK TABLES `politica_de_privacidade` WRITE;
INSERT INTO `politica_de_privacidade` VALUES (1,'<p>a</p><p>a</p><p>a</p><p>aa</p><p>a</p><p>a</p><p>a</p>',NULL,'2022-03-20 23:02:07');
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$D4jBxLgqgJ.O2zMSAbgF1eAKefCi6/cthfAvM3fcSpti.nJ6FVYqm',NULL,NULL,NULL);
UNLOCK TABLES;

--
-- Table structure for table `certificados`
--

DROP TABLE IF EXISTS `certificados`;
CREATE TABLE `certificados` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hash_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_emissao` date NOT NULL,
  `data_validade` date NOT NULL,
  `curso_id` bigint(20) unsigned NOT NULL,
  `instituicao_id` bigint(20) unsigned NOT NULL,
  `cidade` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instrutor_id` bigint(20) unsigned NOT NULL,
  `equipe` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aviso_vencimento_enviado` tinyint(1) NOT NULL DEFAULT 0,
  `data_aviso_vencimento_enviado` date DEFAULT NULL,
  `status_emails` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nao_enviados',
  `data_status_emails_enviados` date DEFAULT NULL,
  `instituicao_parceira` tinyint(1) NOT NULL DEFAULT 0,
  `nome_instituicao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_instituicao` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `texto_instituicao` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `certificados_hash_id_unique` (`hash_id`),
  KEY `certificados_curso_id_foreign` (`curso_id`),
  KEY `certificados_instituicao_id_foreign` (`instituicao_id`),
  KEY `certificados_instrutor_id_foreign` (`instrutor_id`),
  CONSTRAINT `certificados_curso_id_foreign` FOREIGN KEY (`curso_id`) REFERENCES `cursos` (`id`) ON DELETE CASCADE,
  CONSTRAINT `certificados_instituicao_id_foreign` FOREIGN KEY (`instituicao_id`) REFERENCES `instituicoes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `certificados_instrutor_id_foreign` FOREIGN KEY (`instrutor_id`) REFERENCES `instrutores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `certificados`
--

LOCK TABLES `certificados` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `alunos_certificados`
--

DROP TABLE IF EXISTS `alunos_certificados`;
CREATE TABLE `alunos_certificados` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hash_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aluno_id` bigint(20) unsigned NOT NULL,
  `certificado_id` bigint(20) unsigned NOT NULL,
  `nota_prova_teorica` int(11) NOT NULL,
  `nota_prova_pratica` int(11) NOT NULL,
  `nota_final` int(11) NOT NULL,
  `certificado_emitido` tinyint(1) NOT NULL DEFAULT 0,
  `data_envio_certificado` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alunos_certificados_hash_id_unique` (`hash_id`),
  KEY `alunos_certificados_aluno_id_foreign` (`aluno_id`),
  KEY `alunos_certificados_certificado_id_foreign` (`certificado_id`),
  CONSTRAINT `alunos_certificados_aluno_id_foreign` FOREIGN KEY (`aluno_id`) REFERENCES `alunos` (`id`) ON DELETE CASCADE,
  CONSTRAINT `alunos_certificados_certificado_id_foreign` FOREIGN KEY (`certificado_id`) REFERENCES `certificados` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alunos_certificados`
--

LOCK TABLES `alunos_certificados` WRITE;
UNLOCK TABLES;

-- ALTERAÇÃO 30/03/2022
alter table `alunos_certificados` add column `qr_code` varchar(255) COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL after `data_envio_certificado`;


-- Dump completed on 2022-03-20 20:13:43
